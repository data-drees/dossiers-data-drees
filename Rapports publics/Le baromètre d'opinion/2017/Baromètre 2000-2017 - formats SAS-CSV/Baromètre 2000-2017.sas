/* programme qui cr�e une base avec formats appliqu�s dans la library work */

/* library dans laquelle est stock�e la base du barom�tre */
libname lib "REMPLIR" ;

PROC FORMAT;
value sdsexef 
     1 = "Homme" 
     2 = "Femme" 
     3 = "(Sans r�ponse)" 
;

value habitatf 
     1 = "Rural" 
     2 = "Moins de 20 000 habitants" 
     3 = "De 20 000 � 99 999 habitants" 
     4 = "100 000 habitants et plus" 
     5 = "Agglom�ration parisienne" 
;

value sdagetrf 
     1 = "18 � 24 ans" 
     2 = "25 � 34 ans" 
     3 = "35 � 49 ans" 
     4 = "50 � 64 ans" 
     5 = "65 ans et +" 
     6 = "(Sans r�ponse)" 
;

value sdsituaf 
     1 = "Vous travaillez � temps plein" 
     2 = "Vous travaillez � temps partiel" 
     3 = "Vous travaillez de fa�on intermittente" 
     4 = "Vous �tes � la recherche d'un emploi" 
     5 = "Vous �tes �tudiant(e)" 
     6 = "Vous �tes retrait�(e) ou pr�retrait�(e)" 
     7 = "Vous n'exercez aucune activit� professionnelle" 
     8 = "Vous n'exercez aucune activit� professionnelle ou �tes retrait� (avant 2014)" 
;

value sdactf 
     1 = "Salari�.e du secteur priv�" 
     2 = "Salari�.e du secteur public" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Ind�pendant.e employeur.euse" 
     5 = "A la recherche d'un premier emploi" 
     6 = "�l�ve, �tudiant.e, en formation, ou en stage non r�mun�r�" 
     7 = "Apprenti.e sous contrat ou stagiaire r�mun�r�.e" 
     8 = "Au foyer" 
     9 = "Autre inactif.ive" 
     10 = "(NSP)" 
;

value sdstatf 
     1 = "Salari�.e du secteur public" 
     2 = "Salari�.e du secteur priv�" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Employeur.euse" 
     5 = "Ch�meur.euse" 
     6 = "Inactif.ive" 
     7 = "Autre" 
     8 = "(NSP)" 
;

value sdpcs7f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Autre inactif.ive" 
     8 = "(Sans r�ponse)" 
;

value sdpcs10f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Ch�meur.euse" 
     8 = "Retrait�.e" 
     9 = "Au foyer" 
     10 = "Autre inactif.ive" 
     11 = "(Sans r�ponse)" 
;

value sdstatempf 
     1 = "CDI (ou fonctionnaire titulaire, y compris fonctionnaire stagiaire)" 
     2 = "CDD" 
     3 = "int�rim" 
     4 = "sans contrat" 
     5 = "(NSP)" 
;

value sdsitfamf 
     1 = "vous vivez seul.e" 
     2 = "vous �tes un membre du couple" 
     3 = "vous �tes l'unique parent du foyer" 
     4 = "vous �tes un enfant de la famille" 
     5 = "vous �tes un.e ami.e ou un.e parent.e h�berg�.e par la famille" 
     6 = "vous �tes un.e colocataire" 
     7 = "autres (personnel de maison, ...)" 
;

value sdprf 
     1 = "votre conjoint.e" 
     2 = "Votre p�re (ou de votre m�re si votre p�re ne vit pas au foyer)" 
     3 = "La personne de r�f�rence du m�nage" 
     4 = "Vous-m�me" 
;

value regionf 
     1 = "R�gion parisienne" 
     2 = "Bassin Parisien Est" 
     3 = "Bassin Parisien Ouest" 
     4 = "Nord" 
     5 = "Est" 
     6 = "Ouest" 
     7 = "Sud Ouest" 
     8 = "Sud Est" 
     9 = "M�diterran�e" 
;

value sdprsituaf 
     1 = "Il.elle travaille � temps plein" 
     2 = "Il.elle travaille � temps partiel" 
     3 = "Il.elle travaille de fa�on intermittente" 
     4 = "Il.elle est � la recherche d'un emploi, au ch�mage" 
     5 = "Il.elle est �tudiant.e" 
     6 = "Il.elle est retrait�.e ou pr�retrait�.e" 
     7 = "Il.elle n'exerce aucune activit� professionnelle" 
;

value sdpractf 
     1 = "Salari�.e du secteur priv�" 
     2 = "Salari�.e du secteur public" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Ind�pendant.e employeur.euse" 
     5 = "A la recherche d'un premier emploi" 
     6 = "�l�ve, �tudiant.e, en formation, ou en stage non r�mun�r�" 
     7 = "Apprenti.e sous contrat ou stagiaire r�mun�r�.e" 
     8 = "Au foyer" 
     9 = "Autre inactif.ive" 
     10 = "(NSP)" 
;

value sdprstatf 
     1 = "Salari�.e du secteur public" 
     2 = "Salari�.e du secteur priv�" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Employeur.euse" 
     5 = "Ch�meur.euse" 
     6 = "Inactif.ive" 
     7 = "Autre" 
     8 = "(NSP)" 
;

value sdprpcs7f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Autre inactif.ive" 
     8 = "(Sans r�ponse)" 
;

value sdprpcs10f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Ch�meur.euse" 
     8 = "Retrait�.e" 
     9 = "Au foyer" 
     10 = "Autre inactif.ive" 
     11 = "(Sans r�ponse)" 
;

value sdnbpersf 
     1 = "1 personne" 
     2 = "2 personnes" 
     3 = "3 personnes" 
     4 = "4 personnes" 
     5 = "5 personnes" 
     6 = "6 personnes" 
     7 = "7 personnes" 
     8 = "8 personnes" 
     9 = "9 personnes et plus" 
     10 = "10 personnes et plus (depuis 2013)" 
;

value sdcouplef 
     1 = "Oui" 
     2 = "Non" 
;

value sdmatrif 
     1 = "Mari�.e, pacs�.e ou en concubinage" 
     2 = "Veuf ou veuve" 
     3 = "C�libataire" 
;

value sdmatri_nwf 
     1 = "Mari�.e, pacs�.e" 
     2 = "En union libre ou en concubinage" 
     3 = "Veuf ou veuve" 
     4 = "C�libataire" 
;

value sdnbaduf 
     1 = "1" 
     2 = "2" 
     3 = "3" 
     4 = "4" 
     5 = "5" 
     6 = "6" 
     7 = "7" 
     8 = "8" 
     9 = "9" 
     10 = "10 et +" 
;

value sdsplitf 
     1 = "Questionnaire A" 
     2 = "Questionnaire B" 
     3 = "Questionnaire C" 
     4 = "Questionnaire D" 
;

value sdmutf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og1f 
     1 = "Tr�s bonne" 
     2 = "Assez bonne" 
     3 = "Assez mauvaise" 
     4 = "Tr�s mauvaise" 
     5 = "(NSP)" 
;

value og2_abf 
     1 = "Bien meilleure" 
     2 = "Plut�t meilleure" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bonne" 
     5 = "Bien moins bonne" 
     6 = "(NSP)" 
;

value og2_cdf 
     1 = "Bien meilleure" 
     2 = "Plut�t meilleure" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bonne" 
     5 = "Bien moins bonne" 
     6 = "(NSP)" 
;

value og3_1f 
     1 = "Tr�s optimiste" 
     2 = "Plut�t optimiste" 
     3 = "Plut�t pessimiste" 
     4 = "Tr�s pessimiste" 
     5 = "(NSP)" 
;

value og3_2f 
     1 = "Tr�s optimiste" 
     2 = "Plut�t optimiste" 
     3 = "Plut�t pessimiste" 
     4 = "Tr�s pessimiste" 
     5 = "(NSP)" 
;

value og4_1f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_2f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_3f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_4f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_5f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_6f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_7f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_8f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_9f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_1f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_2f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_3f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_4f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_5f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_6f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_7f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og6f 
     1 = "Il faut radicalement changer la soci�t� fran�aise" 
     2 = "Il faut r�former la soci�t� fran�aise sur certains points tout en en conservant l'essentiel" 
     3 = "Il faut conserver la soci�t� fran�aise en l'�tat" 
     4 = "(NSP)" 
;

value og7f 
     1 = "Trop" 
     2 = "Ce qu'il faut" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value og9_abf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og8_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_4f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_8f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_9f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_10f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_11f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og9_cdf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value in1f 
     1 = "Plut�t juste" 
     2 = "Plut�t injuste" 
     3 = "(NSP)" 
;

value in2f 
     1 = "Ont plut�t augment�" 
     2 = "Ont plut�t diminu�" 
     3 = "(Sont rest�es stables)" 
     4 = "(NSP)" 
;

value in3f 
     1 = "Vont plut�t augmenter" 
     2 = "Vont plut�t diminuer" 
     3 = "(Resteront stables)" 
     4 = "(NSP)" 
;

value in4_abf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "(NSP)" 
;

value in4_cdf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "(NSP)" 
     10 = "Les in�galit�s entre les femmes et les hommes" 
;

value in5_abf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "(NSP)" 
;

value in5_cdf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "Les in�galit�s entre les femmes et les hommes" 
     10 = "(NSP)" 
;

value in6f 
     1 = "Tr�s importantes" 
     2 = "Assez importantes" 
     3 = "Assez faibles" 
     4 = "Tr�s faibles" 
     5 = "(NSP)" 
;

value pe1f 
     1 = "Ont diminu�" 
     2 = "Ont augment�" 
     3 = "(Sont rest�es stables)" 
     4 = "(NSP)" 
;

value pe2f 
     1 = "Vont plut�t diminuer" 
     2 = "Vont plut�t augmenter" 
     3 = "(Resteront stables)" 
     4 = "(NSP)" 
;

value pe3f 
     1 = "Oui, plut�t" 
     2 = "Non, plut�t pas" 
     3 = "Je me consid�re d�j� comme pauvre" 
     4 = "(NSP)" 
;

value pe9f 
     1 = "Il faut augmenter le RSA" 
     2 = "Il faut diminuer le RSA" 
     3 = "(Il faut laisser le RSA � ce niveau)" 
     4 = "(NSP)" 
;

value pe10f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe12f 
     1 = "Il faut augmenter le SMIC" 
     2 = "Il faut diminuer le SMIC" 
     3 = "Il faut laisser le SMIC � ce niveau" 
     4 = "(NSP)" 
;

value lo1f 
     1 = "Propri�taire" 
     2 = "Locataire d'un logement social (y compris HLM)" 
     3 = "Locataire hors logement social, c'est-�-dire dans le parc priv�" 
     4 = "Log�.e gratuitement" 
     5 = "(Autres/NSP)" 
     6 = "Locataire (item pour historique)" 
;

value lo2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value lo3f 
     1 = "Sont trop �lev�s pour obtenir un logement social" 
     2 = "Ne sont pas trop �lev�s pour obtenir un logement social" 
     3 = "(NSP)" 
;

value lo4f 
     1 = "Aux familles avec enfants � revenus modestes" 
     2 = "Aux personnes en grande difficult�" 
     3 = "Plus largement, � l�ensemble des cat�gories moyennes" 
     4 = "(NSP)" 
;

value lo9f 
     1 = "Il faut les obliger � d�m�nager pour permettre � ceux qui en ont vraiment besoin d'en b�n�ficier" 
     2 = "On peut leur permettre de rester pour privil�gier la mixit� sociale, tout en leur faisant payer des loyers plus �lev" 
     3 = "(NSP)" 
;

value re1f 
     1 = "Bien meilleur" 
     2 = "Plut�t meilleur" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bon" 
     5 = "Bien moins bon" 
     6 = "(NSP)" 
;

value re2f 
     1 = "Bien meilleur" 
     2 = "Plut�t meilleur" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bon" 
     5 = "Bien moins bon" 
     6 = "(NSP)" 
;

value re3f 
     1 = "Aucune baisse de revenu" 
     2 = "-10%" 
     3 = "-20%" 
     4 = "-30%" 
     5 = "- 40 % ou plus" 
     6 = "(NSP)" 
;

value re7f 
     1 = "Oui, pour les deux parents" 
     2 = "Oui, mais seulement pour les m�res" 
     3 = "Non" 
     4 = "(NSP)" 
;

value re8f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value re9bisf 
     1 = "Les r�formes d�j� effectu�es sont suffisantes" 
     2 = "Des r�formes suppl�mentaires sont n�cessaires" 
     3 = "(NSP)" 
;

value re9f 
     1 = "Il faudrait allonger la dur�e de cotisation" 
     2 = "Il faudrait reculer l'�ge de la retraite" 
     3 = "Il faudrait augmenter les cotisations pesant sur les salari�s" 
     4 = "Il faudrait diminuer les pensions vers�es aux retrait�s" 
     5 = "(Il faudrait un autre type de r�forme)" 
     6 = "(NSP)" 
;

value re10f 
     1 = "Il faut garder le syst�me actuel fond� sur la r�partition, en le r�formant" 
     2 = "Il faut ajouter au syst�me actuel un compl�ment d'assurance ou d'�pargne individuelle" 
     3 = "Il faut remplacer le syst�me actuel par un syst�me d'assurance ou d'�pargne individuelle" 
     4 = "(NSP)" 
     5 = "(Aucune de ces 2 propositions)" 
     6 = "(NSP / Aucune de ces 2 propositions (item pour historique))" 
;

value re11f 
     1 = "Il doit y avoir des r�gimes diff�rents pour tenir compte des diff�rences de statut professionnel" 
     2 = "Il devrait y avoir un socle commun de retraite, tout en gardant certaines diff�rences selon le statut professionnel" 
     3 = "Il ne devrait y avoir qu'un seul r�gime de retraite, avec des caract�ristiques identiques pour tous" 
     4 = "(NSP)" 
;

value re12f 
     1 = "Vous souhaitez partir � la retraite d�s que vous pourrez b�n�ficier d'une retraite au taux plein" 
     2 = "Vous souhaitez partir � la retraite plus tard, pour augmenter le montant de vos pensions" 
     3 = "Vous souhaitez partir � la retraite plus t�t, quitte � recevoir un montant de pension plus faible" 
     4 = "(Vous ne savez pas encore)" 
;

value re13f 
     1 = "Aux veufs et veuves qui ont des ressources faibles ou moyennes" 
     2 = "Aux veufs et veuves qui ont encore des enfants � charge" 
     3 = "A tous les veufs et veuves pendant une p�riode limit�e suivant le d�c�s du conjoint" 
     4 = "A tous les veufs et veuves sans limitation de dur�e" 
     5 = "(NSP)" 
;

value re14f 
     1 = "Les personnes qui ont commenc� � travailler plus jeunes doivent avoir la possibilit� de partir � la retraite plus t" 
     2 = "Il est normal que tout le monde parte � la retraite au m�me �ge" 
     3 = "(NSP)" 
;

value re15f 
     1 = "Les cadres et les ouvriers doivent cotiser le m�me nombre d'ann�es pour partir � la retraite" 
     2 = "Les cadres doivent cotiser plus longtemps car ils ont une esp�rance de vie plus longue" 
     3 = "(NSP)" 
;

value fa1f 
     1 = "Soutenir la natalit�" 
     2 = "Rapprocher les niveaux de vie des familles avec enfants et des personnes sans enfant" 
     3 = "Permettre aux familles de mieux se loger" 
     4 = "Permettre une meilleure conciliation entre vie familiale et vie professionnelle" 
     5 = "Rendre les jeunes de plus de 20 ans plus autonomes � l'�gard de leur famille" 
     6 = "(NSP)" 
;

value ha1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value ha2f 
     1 = "... � toutes les personnes se trouvant dans cette situation quel que soit leur niveau de revenu" 
     2 = "... uniquement aux personnes disposant de faibles revenus" 
     3 = "(NSP)" 
;

value ha3f 
     1 = "Il est normal qu�une personne handicap�e ait un revenu minimum sup�rieur � une personne non handicap�e" 
     2 = "Les minima sociaux devraient �tre les m�mes, que la personne soit handicap�e ou non" 
     3 = "(NSP)" 
;

value ha4f 
     1 = "S�est am�lior�e au cours des derni�res ann�es" 
     2 = "S�est d�t�rior�e au cours des derni�res ann�es" 
     3 = "Est rest�e la m�me au cours des derni�res ann�es" 
     4 = "(NSP)" 
;

value ha5_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value ha5_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value ha5_3f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value ha5_4f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value de1f 
     1 = "L'�tat et les pouvoirs publics" 
     2 = "Les enfants ou les familles des personnes �g�es d�pendantes" 
     3 = "Les personnes �g�es d�pendantes elles-m�mes, en �pargnant ou souscrivant une assurance priv�e" 
     4 = "(NSP)" 
;

value de2f 
     1 = "Obligatoire pour tous" 
     2 = "Obligatoire, � partir d'un certain �ge" 
     3 = "Uniquement pour ceux qui le veulent" 
     4 = "(NSP)" 
;

value de3f 
     1 = "...� toutes les personnes se trouvant dans cette situation, quel que soit leur niveau de revenu" 
     2 = "...uniquement aux personnes disposant de faibles revenus" 
     3 = "(NSP)" 
;

value de4f 
     1 = "Tout � fait pr�t.e" 
     2 = "Plut�t pr�t.e" 
     3 = "Plut�t pas pr�t.e" 
     4 = "Pas du tout pr�t.e" 
     5 = "(NSP)" 
;

value de5f 
     1 = "Vous le placeriez dans une institution sp�cialis�e" 
     2 = "Vous l'accueilleriez chez vous" 
     3 = "Vous consacreriez une partie de votre revenu � lui payer des aides de mani�re � ce qu'il reste � son domicile" 
     4 = "Vous feriez en sorte de pouvoir vous en occuper � son domicile" 
     5 = "(NSP)" 
;

value de6f 
     1 = "Tout � fait envisageable" 
     2 = "Plut�t envisageable" 
     3 = "Plut�t pas envisageable" 
     4 = "Pas envisageable du tout" 
     5 = "(NSP)" 
;

value de7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "Non car j'ai moi-m�me besoin d'aide pour ces activit�s" 
     4 = "(NSP)" 
;

value de8f 
     1 = "Moins de 1h" 
     2 = "Entre 1h et 3h" 
     3 = "Entre 3h et 9h" 
     4 = "Entre 9h et 18h" 
     5 = "Plus de 18h" 
     6 = "Je vis avec elle" 
     7 = "(NSP)" 
;

value de9f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value de10f 
     1 = "Permettre un meilleur am�nagement du temps de travail" 
     2 = "Fournir une aide financi�re" 
     3 = "Permettre aux aidants d'�tre form�s" 
     4 = "Ouvrir des centres d�accueil de jour pour recevoir les personnes d�pendantes dans la semaine" 
     5 = "Autre" 
     6 = "(NSP)" 
;

value ps1_1f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "A tous sans distinction de cat�gories sociales et de statut professionnel (ch�meurs, salari�s du secteur priv�, fonc" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps1_2f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "A tous sans distinction de cat�gories sociales et de statut professionnel (ch�meurs, salari�s du secteur priv�, fonc" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps1_3f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "A tous sans distinction de cat�gories sociales et de statut professionnel (ch�meurs, salari�s du secteur priv�, fonc" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps1_4f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "A tous sans distinction de cat�gories sociales et de statut professionnel (ch�meurs, salari�s du secteur priv�, fonc" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps2f 
     1 = "Il est souhaitable que les entreprises cotisent davantage pour la protection sociale" 
     2 = "Il est souhaitable que les entreprises cotisent moins pour la protection sociale" 
     3 = "Les entreprises ne doivent ni plus, ni moins cotiser qu'actuellement" 
     4 = "(NSP)" 
;

value ps3f 
     1 = "Excessif" 
     2 = "Normal" 
     3 = "Insuffisant" 
     4 = "(NSP)" 
;

value ps8f 
     1 = "De l��tat, des collectivit�s locales ou de la S�curit� sociale" 
     2 = "Des individus et des familles" 
     3 = "Des associations" 
     4 = "(NSP)" 
;

value ps11f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value ps12f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value ps13_ab_1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_ab_2f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_ab_3f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_ab_4f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_ab_5f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_ab_6f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_ab_7f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_2f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_3f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_4f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_5f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_6f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_c_7f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_2f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_3f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_4f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_5f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_6f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_d_7f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps15_1f 
     1 = "Totalement d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas d'accord du tout" 
     5 = "(NSP)" 
;

value ps15_2f 
     1 = "Totalement d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas d'accord du tout" 
     5 = "(NSP)" 
;

value ps15_3f 
     1 = "Totalement d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas d'accord du tout" 
     5 = "(NSP)" 
;

value ps18f 
     1 = "Une augmentation des prestations (allocations logement, allocations familiales, minima sociaux,...)" 
     2 = "Le d�veloppement de services (cr�ches, dispositifs de formation, �quipements pour personnes �g�es,...)" 
     3 = "Un meilleur accompagnement vers les droits (agents accompagnateurs dans les services publics, aide � l�utilisation du" 
     4 = "(NSP)" 
;

value ps16f 
     1 = "Prendre des mesures pour r�duire le d�ficit de la S�curit� Sociale" 
     2 = "Maintenir au niveau actuel les prestations sociales et les remboursements d'assurance maladie" 
     3 = "(NSP)" 
;

value sa1f 
     1 = "Tr�s bon" 
     2 = "Bon" 
     3 = "Moyen" 
     4 = "Mauvais" 
     5 = "Tr�s mauvais" 
     6 = "(NSP)" 
;

value sa3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sa4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sa5f 
     1 = "Beaucoup am�lior�" 
     2 = "Un peu am�lior�" 
     3 = "Un peu d�t�rior�" 
     4 = "Beaucoup d�t�rior�" 
     5 = "Est rest� identique" 
     6 = "(NSP)" 
;

value sa6_ab_1f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_2f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_3f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_4f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_5f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_6f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_1f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_2f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_3f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_4f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_5f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_6f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa24f 
     1 = "Car certains de leurs comportements sont mauvais pour la sant�" 
     2 = "En raison de l�environnement dans lequel ils vivent" 
     3 = "� cause de leurs conditions de travail" 
     4 = "Pour des raisons g�n�tiques" 
     5 = "Parce qu�ils sont pauvres" 
     6 = "(NSP)" 
;

value sa7_1f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_2f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_3f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_4f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_5f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_6f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_7f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa7_8f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
     8 = "Refus de r�pondre" 
;

value sa8_1f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_2f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_3f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_5f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_6f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_7f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_9f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa9_1f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_2f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_3f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_4f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_5f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_6f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa11f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa25f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa13_1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa13_2f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa13_3f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa13_4f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa13_5f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value sa14f 
     1 = "Consulter votre m�decin" 
     2 = "En parler � votre famille, vos amis" 
     3 = "Vous renseigner sur des sites internet sp�cialis�s ou des forums de discussion" 
     4 = "Consulter votre pharmacien, sans passer par le m�decin" 
     5 = "Vous n'avez pas besoin de consulter quelqu'un pour vous soigner (autom�dication)" 
     6 = "(NSP)" 
;

value sa15_1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "Je ne suis jamais all�.e � l�h�pital" 
     6 = "(NSP)" 
;

value sa15_2f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "Je ne vais jamais chez le m�decin" 
     6 = "(NSP)" 
;

value sa16_1f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa16_2f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa16_3f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa17f 
     1 = "Beaucoup trop �lev�" 
     2 = "Un peu trop �lev�" 
     3 = "Un peu trop faible" 
     4 = "Beaucoup trop faible" 
     5 = "(NSP)" 
;

value sa18f 
     1 = "Tr�s �lev�s" 
     2 = "Plut�t �lev�s" 
     3 = "Plut�t faibles" 
     4 = "Ils seront totalement rembours�s" 
     5 = "(NSP)" 
;

value sa19_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_3f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_4f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_5f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_6f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_7f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa19_8f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value sa23f 
     1 = "Tout � fait justifi�s" 
     2 = "Plut�t justifi�s" 
     3 = "Plut�t pas justifi�s" 
     4 = "Pas du tout justifi�s" 
     5 = "(NSP)" 
;

value sa22_1_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_1_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_1_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_1_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_2_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_2_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_2_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_2_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_3_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_3_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_3_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_3_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_4_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_4_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_4_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_4_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_5_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_5_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_5_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_5_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_6_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_6_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_6_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_6_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_7_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_7_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_7_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_7_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_8_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_8_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_8_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sa22_8_4f 
     1 = "Non" 
     2 = "Oui" 
;

value cs1f 
     1 = "Tr�s forte" 
     2 = "Assez forte" 
     3 = "Pas tr�s forte" 
     4 = "Pas du tout forte" 
     5 = "(NSP)" 
;

value cs5f 
     1 = "Tr�s bien int�gr�.e" 
     2 = "Assez bien int�gr�.e" 
     3 = "Pas bien int�gr�.e" 
     4 = "Pas int�gr�.e du tout" 
     5 = "(NSP)" 
;

value cs6f 
     1 = "D'avoir un travail qui vous convient" 
     2 = "D'�tre bien entour� par votre famille ou vos amis" 
     3 = "De partager une culture et des valeurs communes" 
     4 = "D'avoir un r�le de citoyen actif" 
     5 = "D'avoir un niveau de vie convenable" 
     6 = "D'�tre de nationalit� fran�aise" 
     7 = "(NSP)" 
;

value cs17f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs18f 
     1 = "Les pouvoirs publics font trop" 
     2 = "Les pouvoirs publics font ce qu'ils doivent" 
     3 = "Les pouvoirs publics ne font pas assez pour les plus d�munis" 
     4 = "(NSP)" 
;

value cs19_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value cs19_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value cs20f 
     1 = "Par manque d'information sur les aides, ou sur les organismes � qui s'adresser" 
     2 = "Parce que les d�marches � faire sont trop complexes et trop longues" 
     3 = "Parce qu'elles pr�f�rent s'en sortir par elles-m�mes" 
     4 = "Parce qu'elles refusent de d�pendre de l'aide sociale et d'�tre consid�r�es comme des assist�es" 
     5 = "Pour ne pas avoir � rendre des comptes, � faire l'objet de contr�le" 
     6 = "Pour ne pas subir des cons�quences n�gatives (perte d'autres droits, obligation de payer des imp�ts, probl�mes admin" 
     7 = "Parce que ces aides n'apportent pas grand-chose financi�rement" 
     8 = "Autre" 
     9 = "(NSP)" 
;

value cs13f 
     1 = "Veiller � ce que chacun puisse �tre libre de ses croyances et de ses pratiques religieuses" 
     2 = "Veiller � ce que les croyances et les pratiques religieuses des individus ne soient pas visibles dans les espaces publi" 
     3 = "(NSP)" 
;

value cs14f 
     1 = "La diversit� des cultures et des origines est une richesse pour notre pays" 
     2 = "La diversit� des cultures et des origines rend difficile la vie en commun dans notre pays" 
     3 = "(NSP)" 
;

value cs9_abf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs9_cd_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs9_cd_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs9_cd_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_ab_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_ab_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_ab_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_ab_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_ab_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_cd_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_cd_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_cd_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_cd_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs16_cd_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs22f 
     1 = "Entre les diff�rentes g�n�rations" 
     2 = "Entre les diff�rentes cat�gories socio-�conomiques" 
     3 = "Entre les diff�rents partis ou tendances politiques" 
     4 = "Entre les diff�rents territoires" 
     5 = "Entre les diff�rentes confessions religieuses" 
     6 = "Entre les personnes d�origines diff�rentes" 
     7 = "(NSP)" 
;

value cs23f 
     1 = "Moins" 
     2 = "� peu pr�s ce chiffre" 
     3 = "Davantage" 
     4 = "Aucun migrant" 
     5 = "(Autre crit�re de r�partition)" 
     6 = "(NSP)" 
;

value sdassynd_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdassynd_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdrelf 
     1 = "Une pratique religieuse r�guli�re" 
     2 = "Une pratique occasionnelle ou le sentiment d'appartenir � une religion" 
     3 = "Ni pratique ni sentiment d'appartenance" 
     4 = "(NSP ou souhaite ne pas r�pondre)" 
;

value sdproxim_1_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_1_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_1_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_1_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_1_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_2_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_2_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_2_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_2_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_2_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_3_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_3_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_3_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_3_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_3_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_4_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_4_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_4_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_4_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_4_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_5_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_5_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_5_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_5_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_5_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_6_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_6_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_6_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_6_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_6_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_7_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_7_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_7_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_7_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_7_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_8_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_8_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_8_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_8_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_8_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_9_1f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_9_2f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_9_3f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_9_4f 
     1 = "Non" 
     2 = "Oui" 
;

value sdproxim_9_5f 
     1 = "Non" 
     2 = "Oui" 
;

value sdrichomf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_8f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_9f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_10f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_11f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_12f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdrevtrf 
     1 = "Moins de 1000 euros par mois" 
     2 = "De 1000 � moins de 1400 euros par mois" 
     3 = "De 1400 � moins de 1900 euros par mois" 
     4 = "De 1900 � moins de 2400 euros par mois" 
     5 = "De 2400 � moins de 3800 euros par mois" 
     6 = "De 3800 � moins de 5300 euros par mois" 
     7 = "Plus de 5300 euros par mois" 
     8 = "(NSP/refus de r�ponse)" 
;

value sddiplf 
     1 = "Vous n'avez pas de dipl�me" 
     2 = "Vous avez un certificat d'�tudes primaires" 
     3 = "Vous avez un ancien brevet, BEPC, ou brevet des coll�ges" 
     4 = "Vous avez un certificat d'aptitude professionnelle (CAP), un brevet  d'enseignement professionnel (BEP)" 
     5 = "Vous avez un bac d'enseignement g�n�ral" 
     6 = "Vous avez un bac d'enseignement technologique ou professionnel" 
     7 = "Vous avez un bac + 2 ans ou niveau bac + 2 ans (DUT, BTS, DEUG, 'L2')" 
     8 = "Vous avez un dipl�me sup�rieur (2�me, 3�me cycle, grande �cole,'L3', 'M1', 'M2')" 
     9 = "(Autre situation)" 
     10 = "(NSP)" 
;

value sdpolf 
     1 = "0-2 extr�me gauche" 
     2 = "3-4 gauche mod�r�e" 
     3 = "5 centre" 
     4 = "6-7 droite mod�r�e" 
     5 = "8-10 extr�me droite" 
     6 = "(NSP)" 
;

value sdpol_nwf 
     1 = "0 Tr�s � gauche" 
     2 = "1" 
     3 = "2" 
     4 = "3" 
     5 = "4" 
     6 = "5" 
     7 = "6" 
     8 = "7" 
     9 = "8" 
     10 = "9" 
     11 = "10 Tr�s � droite" 
     12 = "(NSP)" 
;

value sdnatf 
     1 = "N�.e fran�ais.e" 
     2 = "Devenu.e fran�ais.e par acquisition (naturalisation, mariage, d�claration, option � votre majorit�...)" 
     3 = "�tranger.�re" 
     4 = "(NSP)" 
;

value sdpnais_detf 
     1 = "ACORES, MADERE" 
     2 = "AFGHANISTAN" 
     3 = "AFRIQUE DU SUD" 
     4 = "ALASKA / ETATS-UNIS D'ALASKA" 
     5 = "ALBANIE" 
     6 = "ALGERIE" 
     7 = "ALLEMAGNE" 
     8 = "ANDORRE" 
     9 = "ANGOLA" 
     10 = "ANGUILLA" 
     11 = "ANTIGUA-ET-BARBUDA" 
     12 = "ANTILLES NEERLANDAISES" 
     13 = "ARABIE SAOUDITE" 
     14 = "ARGENTINE" 
     15 = "ARMENIE" 
     16 = "ARUBA" 
     17 = "AUSTRALIE" 
     18 = "AUTRICHE" 
     19 = "AZERBAIDJAN" 
     20 = "BAHAMAS" 
     21 = "BAHREIN" 
     22 = "BANGLADESH" 
     23 = "BARBADE" 
     24 = "BELGIQUE" 
     25 = "BELIZE" 
     26 = "BENIN" 
     27 = "BERMUDES" 
     28 = "BHOUTAN" 
     29 = "BIELORUSSIE" 
     30 = "BIRMANIE" 
     31 = "BOLIVIE" 
     32 = "BOSNIE-HERZEGOVINE" 
     33 = "BOTSWANA" 
     34 = "BOUVET (ILE)" 
     35 = "BRESIL" 
     36 = "BRUNEI" 
     37 = "BULGARIE" 
     38 = "BURKINA" 
     39 = "BURUNDI" 
     40 = "CAIMANES (ILES)" 
     41 = "CAMBODGE" 
     42 = "CAMEROUN" 
     43 = "CANADA" 
     44 = "CANARIES (ILES)" 
     45 = "CAP-VERT" 
     46 = "CENTRAFRICAINE (REPUBLIQUE)" 
     47 = "CHILI" 
     48 = "CHINE" 
     49 = "CHRISTMAS (ILE)" 
     50 = "CHYPRE" 
     51 = "CLIPPERTON (ILE)" 
     52 = "COCOS ou KEELING (ILES)" 
     53 = "COLOMBIE" 
     54 = "COMORES" 
     55 = "CONGO" 
     56 = "CONGO (REPUBLIQUE DEMOCRATIQUE)" 
     57 = "COOK (ILES)" 
     58 = "COREE" 
     59 = "COREE (REPUBLIQUE DE)" 
     60 = "COREE (REPUBLIQUE POPULAIRE DEMOCRATIQUE DE)" 
     61 = "COSTA RICA" 
     62 = "COTE D'IVOIRE" 
     63 = "CROATIE" 
     64 = "CUBA" 
     65 = "DANEMARK" 
     66 = "DJIBOUTI" 
     67 = "DOMINICAINE (REPUBLIQUE)" 
     68 = "DOMINIQUE" 
     69 = "EGYPTE" 
     70 = "EL SALVADOR" 
     71 = "EMIRATS ARABES UNIS" 
     72 = "EQUATEUR" 
     73 = "ERYTHREE" 
     74 = "ESPAGNE" 
     75 = "ESTONIE" 
     76 = "ETATS MALAIS NON FEDERES" 
     77 = "ETATS-UNIS" 
     78 = "ETHIOPIE" 
     79 = "EX-REPUBLIQUE YOUGOSLAVE DE MACEDOINE / MACEDOINE" 
     80 = "FEROE (ILES)" 
     81 = "FIDJI" 
     82 = "FINLANDE" 
     83 = "FRANCE" 
     84 = "GABON" 
     85 = "GAMBIE" 
     86 = "GEORGIE" 
     87 = "GEORGIE DU SUD ET LES ILES SANDWICH DU SUD" 
     88 = "GHANA" 
     89 = "GIBRALTAR" 
     90 = "GOA" 
     91 = "GRECE" 
     92 = "GRENADE" 
     93 = "GROENLAND" 
     94 = "GUADELOUPE" 
     95 = "GUAM" 
     96 = "GUATEMALA" 
     97 = "GUERNESEY" 
     98 = "GUINEE" 
     99 = "GUINEE EQUATORIALE" 
     100 = "GUINEE-BISSAU" 
     101 = "GUYANA" 
     102 = "GUYANE" 
     103 = "HAITI" 
     104 = "HAWAII (ILES)" 
     105 = "HEARD ET MACDONALD (ILES)" 
     106 = "HONDURAS" 
     107 = "HONG-KONG" 
     108 = "HONGRIE" 
     109 = "ILES PORTUGAISES DE L'OCEAN INDIEN" 
     110 = "INDE" 
     111 = "INDONESIE" 
     112 = "IRAN" 
     113 = "IRAQ" 
     114 = "IRLANDE, ou EIRE" 
     115 = "ISLANDE" 
     116 = "ISRAEL" 
     117 = "ITALIE" 
     118 = "JAMAIQUE" 
     119 = "JAPON" 
     120 = "JERSEY" 
     121 = "JORDANIE" 
     122 = "KAMTCHATKA" 
     123 = "KAZAKHSTAN" 
     124 = "KENYA" 
     125 = "KIRGHIZISTAN" 
     126 = "KIRIBATI" 
     127 = "KOWEIT" 
     128 = "LA REUNION" 
     129 = "LABRADOR" 
     130 = "LAOS" 
     131 = "LESOTHO" 
     132 = "LETTONIE" 
     133 = "LIBAN" 
     134 = "LIBERIA" 
     135 = "LIBYE" 
     136 = "LIECHTENSTEIN" 
     137 = "LITUANIE" 
     138 = "LUXEMBOURG" 
     139 = "MACAO" 
     140 = "MADAGASCAR" 
     141 = "MALAISIE" 
     142 = "MALAWI" 
     143 = "MALDIVES" 
     144 = "MALI" 
     145 = "MALOUINES, OU FALKLAND (ILES)" 
     146 = "MALTE" 
     147 = "MAN (ILE)" 
     148 = "MANDCHOURIE" 
     149 = "MARIANNES DU NORD (ILES)" 
     150 = "MAROC" 
     151 = "MARSHALL (ILES)" 
     152 = "MARTINIQUE" 
     153 = "MAURICE" 
     154 = "MAURITANIE" 
     155 = "MAYOTTE" 
     156 = "MEXIQUE" 
     157 = "MICRONESIE (ETATS FEDERES DE)" 
     158 = "MOLDAVIE" 
     159 = "MONACO" 
     160 = "MONGOLIE" 
     161 = "MONTENEGRO" 
     162 = "MONTSERRAT" 
     163 = "MOZAMBIQUE" 
     164 = "NAMIBIE" 
     165 = "NAURU" 
     166 = "NEPAL" 
     167 = "NICARAGUA" 
     168 = "NIGER" 
     169 = "NIGERIA" 
     170 = "NIUE" 
     171 = "NORFOLK (ILE)" 
     172 = "NORVEGE" 
     173 = "NOUVELLE-CALEDONIE" 
     174 = "NOUVELLE-ZELANDE" 
     175 = "OCEAN INDIEN (TERRITOIRE BRITANNIQUE DE L')" 
     176 = "OMAN" 
     177 = "OUGANDA" 
     178 = "OUZBEKISTAN" 
     179 = "PAKISTAN" 
     180 = "PALAOS (ILES)" 
     181 = "PALESTINE" 
     182 = "PANAMA" 
     183 = "PAPOUASIE-NOUVELLE-GUINEE" 
     184 = "PARAGUAY" 
     185 = "PAYS-BAS" 
     186 = "PEROU" 
     187 = "PHILIPPINES" 
     188 = "PITCAIRN (ILE)" 
     189 = "POLOGNE" 
     190 = "POLYNESIE FRANCAISE" 
     191 = "PORTO RICO" 
     192 = "PORTUGAL" 
     193 = "POSSESSIONS BRITANNIQUES AU PROCHE-ORIENT" 
     194 = "PRESIDES" 
     195 = "PROVINCES ESPAGNOLES D'AFRIQUE" 
     196 = "QATAR" 
     197 = "REPUBLIQUE DEMOCRATIQUE ALLEMANDE" 
     198 = "REPUBLIQUE FEDERALE D'ALLEMAGNE" 
     199 = "ROUMANIE" 
     200 = "ROYAUME-UNI" 
     201 = "RUSSIE / FEDERATION DE RUSSIE" 
     202 = "RWANDA" 
     203 = "SAHARA OCCIDENTAL" 
     204 = "SAINT-BARTHELEMY" 
     205 = "SAINT-CHRISTOPHE-ET-NIEVES" 
     206 = "SAINTE-HELENE" 
     207 = "SAINTE-LUCIE" 
     208 = "SAINT-MARIN" 
     209 = "SAINT-MARTIN" 
     210 = "SAINT-PIERRE-ET-MIQUELON" 
     211 = "SAINT-VINCENT-ET-LES GRENADINES" 
     212 = "SALOMON (ILES)" 
     213 = "SAMOA AMERICAINES" 
     214 = "SAMOA OCCIDENTALES" 
     215 = "SAO TOME-ET-PRINCIPE" 
     216 = "SENEGAL" 
     217 = "SERBIE" 
     218 = "SEYCHELLES" 
     219 = "SIBERIE" 
     220 = "SIERRA LEONE" 
     221 = "SINGAPOUR" 
     222 = "SLOVAQUIE" 
     223 = "SLOVENIE" 
     224 = "SOMALIE" 
     225 = "SOUDAN" 
     226 = "SOUDAN ANGLO-EGYPTIEN, KENYA, OUGANDA" 
     227 = "SRI LANKA" 
     228 = "SUEDE" 
     229 = "SUISSE" 
     230 = "SURINAME" 
     231 = "SVALBARD et ILE JAN MAYEN" 
     232 = "SWAZILAND" 
     233 = "SYRIE" 
     234 = "TADJIKISTAN" 
     235 = "TAIWAN" 
     236 = "TANGER" 
     237 = "TANZANIE" 
     238 = "TCHAD" 
     239 = "TCHECOSLOVAQUIE" 
     240 = "TCHEQUE (REPUBLIQUE)" 
     241 = "TERR. DES ETATS-UNIS D'AMERIQUE EN AMERIQUE" 
     242 = "TERR. DES ETATS-UNIS D'AMERIQUE EN OCEANIE" 
     243 = "TERR. DU ROYAUME-UNI DANS L'ATLANTIQUE SUD" 
     244 = "TERRE-NEUVE" 
     245 = "TERRES AUSTRALES FRANCAISES" 
     246 = "TERRITOIRE DES PAYS-BAS" 
     247 = "TERRITOIRES DU ROYAUME-UNI AUX ANTILLES" 
     248 = "THAILANDE" 
     249 = "TIMOR ORIENTAL" 
     250 = "TOGO" 
     251 = "TOKELAU" 
     252 = "TONGA" 
     253 = "TRINITE-ET-TOBAGO" 
     254 = "TUNISIE" 
     255 = "TURKESTAN RUSSE" 
     256 = "TURKMENISTAN" 
     257 = "TURKS ET CAIQUES (ILES)" 
     258 = "TURQUIE" 
     259 = "TURQUIE D'EUROPE" 
     260 = "TUVALU" 
     261 = "UKRAINE" 
     262 = "URUGUAY" 
     263 = "VANUATU" 
     264 = "VATICAN, ou SAINT-SIEGE" 
     265 = "VENEZUELA" 
     266 = "VIERGES BRITANNIQUES (ILES)" 
     267 = "VIERGES DES ETATS-UNIS (ILES)" 
     268 = "VIET NAM" 
     269 = "VIET NAM DU NORD" 
     270 = "VIET NAM DU SUD" 
     271 = "WALLIS-ET-FUTUNA" 
     272 = "YEMEN" 
     273 = "YEMEN (REPUBLIQUE ARABE DU)" 
     274 = "YEMEN DEMOCRATIQUE" 
     275 = "ZAMBIE" 
     276 = "ZANZIBAR" 
     277 = "ZIMBABWE" 
     278 = "Autre : Pr�ciser" 
     279 = "279" 
     280 = "280" 
     281 = "281" 
     282 = "282" 
     283 = "283" 
     284 = "284" 
     285 = "285" 
     286 = "286" 
     287 = "287" 
     288 = "288" 
     289 = "289" 
     290 = "290" 
     291 = "BELARUS" 
     292 = "HUTT RIVER" 
     293 = "MYANMAR" 
     294 = "SAINT-KITTS-ET-NEVIS" 
     295 = "SAMOA" 
     296 = "SEBORGA" 
     297 = "TIBET" 
     298 = "YOUGOSLAVIE" 
     299 = "299" 
     300 = "300" 
     301 = "301" 
     302 = "302" 
     303 = "303" 
     304 = "304" 
     305 = "305" 
     306 = "306" 
     307 = "307" 
     308 = "308" 
     309 = "309" 
     310 = "310" 
     311 = "311" 
     312 = "312" 
     313 = "313" 
     314 = "314" 
     315 = "315" 
     316 = "316" 
     317 = "317" 
     318 = "318" 
     319 = "319" 
     320 = "320" 
     321 = "321" 
     322 = "322" 
     323 = "323" 
     324 = "324" 
     325 = "325" 
     326 = "326" 
     327 = "327" 
     328 = "328" 
     329 = "329" 
     330 = "330" 
     331 = "331" 
     332 = "332" 
     333 = "333" 
     334 = "334" 
     335 = "335" 
     336 = "336" 
     337 = "337" 
     338 = "338" 
     339 = "339" 
     340 = "340" 
     341 = "341" 
     342 = "342" 
     343 = "343" 
     344 = "344" 
     345 = "345" 
     346 = "346" 
     347 = "347" 
     348 = "348" 
     349 = "349" 
     350 = "350" 
     351 = "351" 
     352 = "352" 
     353 = "353" 
     354 = "354" 
     355 = "355" 
     356 = "356" 
     357 = "357" 
     358 = "358" 
     359 = "359" 
     360 = "360" 
     361 = "361" 
     362 = "362" 
     363 = "363" 
     364 = "364" 
     365 = "365" 
     366 = "366" 
     367 = "367" 
     368 = "368" 
     369 = "369" 
     370 = "370" 
     371 = "371" 
     372 = "372" 
     373 = "373" 
     374 = "374" 
     375 = "375" 
     376 = "376" 
     377 = "377" 
     378 = "378" 
     379 = "379" 
     380 = "380" 
     381 = "381" 
     382 = "382" 
     383 = "383" 
     384 = "384" 
     385 = "385" 
     386 = "386" 
     387 = "387" 
     388 = "388" 
     389 = "389" 
     390 = "390" 
     391 = "391" 
     392 = "392" 
     393 = "393" 
     394 = "394" 
     395 = "395" 
     396 = "396" 
     397 = "397" 
     398 = "398" 
     399 = "399" 
     400 = "400" 
     401 = "401" 
     402 = "402" 
     403 = "403" 
     404 = "404" 
     405 = "405" 
     406 = "406" 
     407 = "407" 
     408 = "408" 
     409 = "409" 
     410 = "410" 
     411 = "411" 
     412 = "412" 
     413 = "413" 
     414 = "414" 
     415 = "415" 
     416 = "416" 
     417 = "417" 
     418 = "418" 
     419 = "419" 
     420 = "420" 
     421 = "421" 
     422 = "422" 
     423 = "423" 
     424 = "424" 
     425 = "425" 
     426 = "426" 
     427 = "427" 
     428 = "428" 
     429 = "429" 
     430 = "430" 
     431 = "431" 
     432 = "432" 
     433 = "433" 
     434 = "434" 
     435 = "435" 
     436 = "436" 
     437 = "437" 
     438 = "438" 
     439 = "439" 
     440 = "440" 
     441 = "441" 
     442 = "442" 
     443 = "443" 
     444 = "444" 
     445 = "445" 
     446 = "446" 
     447 = "447" 
     448 = "448" 
     449 = "449" 
     450 = "450" 
     451 = "451" 
     452 = "452" 
     453 = "453" 
     454 = "454" 
     455 = "455" 
     456 = "456" 
     457 = "457" 
     458 = "458" 
     459 = "459" 
     460 = "460" 
     461 = "461" 
     462 = "462" 
     463 = "463" 
     464 = "464" 
     465 = "465" 
     466 = "466" 
     467 = "467" 
     468 = "468" 
     469 = "469" 
     470 = "470" 
     471 = "471" 
     472 = "472" 
     473 = "473" 
     474 = "474" 
     475 = "475" 
     476 = "476" 
     477 = "477" 
     478 = "478" 
     479 = "479" 
     480 = "480" 
     481 = "481" 
     482 = "482" 
     483 = "483" 
     484 = "484" 
     485 = "485" 
     486 = "486" 
     487 = "487" 
     488 = "488" 
     489 = "489" 
     490 = "490" 
     491 = "491" 
     492 = "492" 
     493 = "493" 
     494 = "494" 
     495 = "495" 
     496 = "496" 
     497 = "497" 
     498 = "498" 
     499 = "499" 
     500 = "500" 
     501 = "501" 
     502 = "502" 
     503 = "503" 
     504 = "504" 
     505 = "505" 
     506 = "506" 
     507 = "507" 
     508 = "508" 
     509 = "509" 
     510 = "510" 
     511 = "511" 
     512 = "512" 
     513 = "513" 
     514 = "514" 
     515 = "515" 
     516 = "516" 
     517 = "517" 
     518 = "518" 
     519 = "519" 
     520 = "520" 
     521 = "521" 
     522 = "522" 
     523 = "523" 
     524 = "524" 
     525 = "525" 
     526 = "526" 
     527 = "527" 
     528 = "528" 
     529 = "529" 
     530 = "530" 
     531 = "531" 
     532 = "532" 
     533 = "533" 
     534 = "534" 
     535 = "535" 
     536 = "536" 
     537 = "537" 
     538 = "538" 
     539 = "539" 
     540 = "540" 
     541 = "541" 
     542 = "542" 
     543 = "543" 
     544 = "544" 
     545 = "545" 
     546 = "546" 
     547 = "547" 
     548 = "548" 
     549 = "549" 
     550 = "550" 
     551 = "551" 
     552 = "552" 
     553 = "553" 
     554 = "554" 
     555 = "555" 
     556 = "556" 
     557 = "557" 
     558 = "558" 
     559 = "559" 
     560 = "560" 
     561 = "561" 
     562 = "562" 
     563 = "563" 
     564 = "564" 
     565 = "565" 
     566 = "566" 
     567 = "567" 
     568 = "568" 
     569 = "569" 
     570 = "570" 
     571 = "571" 
     572 = "572" 
     573 = "573" 
     574 = "574" 
     575 = "575" 
     576 = "576" 
     577 = "577" 
     578 = "578" 
     579 = "579" 
     580 = "580" 
     581 = "581" 
     582 = "582" 
     583 = "583" 
     584 = "584" 
     585 = "585" 
     586 = "586" 
     587 = "587" 
     588 = "588" 
     589 = "589" 
     590 = "590" 
     591 = "591" 
     592 = "592" 
     593 = "593" 
     594 = "594" 
     595 = "595" 
     596 = "596" 
     597 = "597" 
     598 = "598" 
     599 = "599" 
     600 = "600" 
     601 = "601" 
     602 = "602" 
     603 = "603" 
     604 = "604" 
     605 = "605" 
     606 = "606" 
     607 = "607" 
     608 = "608" 
     609 = "609" 
     610 = "610" 
     611 = "611" 
     612 = "612" 
     613 = "613" 
     614 = "614" 
     615 = "615" 
     616 = "616" 
     617 = "617" 
     618 = "618" 
     619 = "619" 
     620 = "620" 
     621 = "621" 
     622 = "622" 
     623 = "623" 
     624 = "624" 
     625 = "625" 
     626 = "626" 
     627 = "627" 
     628 = "628" 
     629 = "629" 
     630 = "630" 
     631 = "631" 
     632 = "632" 
     633 = "633" 
     634 = "634" 
     635 = "635" 
     636 = "636" 
     637 = "637" 
     638 = "638" 
     639 = "639" 
     640 = "640" 
     641 = "641" 
     642 = "642" 
     643 = "643" 
     644 = "644" 
     645 = "645" 
     646 = "646" 
     647 = "647" 
     648 = "648" 
     649 = "649" 
     650 = "650" 
     651 = "651" 
     652 = "652" 
     653 = "653" 
     654 = "654" 
     655 = "655" 
     656 = "656" 
     657 = "657" 
     658 = "658" 
     659 = "659" 
     660 = "660" 
     661 = "661" 
     662 = "662" 
     663 = "663" 
     664 = "664" 
     665 = "665" 
     666 = "666" 
     667 = "667" 
     668 = "668" 
     669 = "669" 
     670 = "670" 
     671 = "671" 
     672 = "672" 
     673 = "673" 
     674 = "674" 
     675 = "675" 
     676 = "676" 
     677 = "677" 
     678 = "678" 
     679 = "679" 
     680 = "680" 
     681 = "681" 
     682 = "682" 
     683 = "683" 
     684 = "684" 
     685 = "685" 
     686 = "686" 
     687 = "687" 
     688 = "688" 
     689 = "689" 
     690 = "690" 
     691 = "691" 
     692 = "692" 
     693 = "693" 
     694 = "694" 
     695 = "695" 
     696 = "696" 
     697 = "697" 
     698 = "698" 
     699 = "699" 
     700 = "700" 
     701 = "701" 
     702 = "702" 
     703 = "703" 
     704 = "704" 
     705 = "705" 
     706 = "706" 
     707 = "707" 
     708 = "708" 
     709 = "709" 
     710 = "710" 
     711 = "711" 
     712 = "712" 
     713 = "713" 
     714 = "714" 
     715 = "715" 
     716 = "716" 
     717 = "717" 
     718 = "718" 
     719 = "719" 
     720 = "720" 
     721 = "721" 
     722 = "722" 
     723 = "723" 
     724 = "724" 
     725 = "725" 
     726 = "726" 
     727 = "727" 
     728 = "728" 
     729 = "729" 
     730 = "730" 
     731 = "731" 
     732 = "732" 
     733 = "733" 
     734 = "734" 
     735 = "735" 
     736 = "736" 
     737 = "737" 
     738 = "738" 
     739 = "739" 
     740 = "740" 
     741 = "741" 
     742 = "742" 
     743 = "743" 
     744 = "744" 
     745 = "745" 
     746 = "746" 
     747 = "747" 
     748 = "748" 
     749 = "749" 
     750 = "750" 
     751 = "751" 
     752 = "752" 
     753 = "753" 
     754 = "754" 
     755 = "755" 
     756 = "756" 
     757 = "757" 
     758 = "758" 
     759 = "759" 
     760 = "760" 
     761 = "761" 
     762 = "762" 
     763 = "763" 
     764 = "764" 
     765 = "765" 
     766 = "766" 
     767 = "767" 
     768 = "768" 
     769 = "769" 
     770 = "770" 
     771 = "771" 
     772 = "772" 
     773 = "773" 
     774 = "774" 
     775 = "775" 
     776 = "776" 
     777 = "777" 
     778 = "778" 
     779 = "779" 
     780 = "780" 
     781 = "781" 
     782 = "782" 
     783 = "783" 
     784 = "784" 
     785 = "785" 
     786 = "786" 
     787 = "787" 
     788 = "788" 
     789 = "789" 
     790 = "790" 
     791 = "791" 
     792 = "792" 
     793 = "793" 
     794 = "794" 
     795 = "795" 
     796 = "796" 
     797 = "797" 
     798 = "798" 
     799 = "799" 
     800 = "800" 
     801 = "801" 
     802 = "802" 
     803 = "803" 
     804 = "804" 
     805 = "805" 
     806 = "806" 
     807 = "807" 
     808 = "808" 
     809 = "809" 
     810 = "810" 
     811 = "811" 
     812 = "812" 
     813 = "813" 
     814 = "814" 
     815 = "815" 
     816 = "816" 
     817 = "817" 
     818 = "818" 
     819 = "819" 
     820 = "820" 
     821 = "821" 
     822 = "822" 
     823 = "823" 
     824 = "824" 
     825 = "825" 
     826 = "826" 
     827 = "827" 
     828 = "828" 
     829 = "829" 
     830 = "830" 
     831 = "831" 
     832 = "832" 
     833 = "833" 
     834 = "834" 
     835 = "835" 
     836 = "836" 
     837 = "837" 
     838 = "838" 
     839 = "839" 
     840 = "840" 
     841 = "841" 
     842 = "842" 
     843 = "843" 
     844 = "844" 
     845 = "845" 
     846 = "846" 
     847 = "847" 
     848 = "848" 
     849 = "849" 
     850 = "850" 
     851 = "851" 
     852 = "852" 
     853 = "853" 
     854 = "854" 
     855 = "855" 
     856 = "856" 
     857 = "857" 
     858 = "858" 
     859 = "859" 
     860 = "860" 
     861 = "861" 
     862 = "862" 
     863 = "863" 
     864 = "864" 
     865 = "865" 
     866 = "866" 
     867 = "867" 
     868 = "868" 
     869 = "869" 
     870 = "870" 
     871 = "871" 
     872 = "872" 
     873 = "873" 
     874 = "874" 
     875 = "875" 
     876 = "876" 
     877 = "877" 
     878 = "878" 
     879 = "879" 
     880 = "880" 
     881 = "881" 
     882 = "882" 
     883 = "883" 
     884 = "884" 
     885 = "885" 
     886 = "886" 
     887 = "887" 
     888 = "888" 
     889 = "889" 
     890 = "890" 
     891 = "891" 
     892 = "892" 
     893 = "893" 
     894 = "894" 
     895 = "895" 
     896 = "896" 
     897 = "897" 
     898 = "898" 
     899 = "899" 
     900 = "900" 
     901 = "901" 
     902 = "902" 
     903 = "903" 
     904 = "904" 
     905 = "905" 
     906 = "906" 
     907 = "907" 
     908 = "908" 
     909 = "909" 
     910 = "910" 
     911 = "911" 
     912 = "912" 
     913 = "913" 
     914 = "914" 
     915 = "915" 
     916 = "916" 
     917 = "917" 
     918 = "918" 
     919 = "919" 
     920 = "920" 
     921 = "921" 
     922 = "922" 
     923 = "923" 
     924 = "924" 
     925 = "925" 
     926 = "926" 
     927 = "927" 
     928 = "928" 
     929 = "929" 
     930 = "930" 
     931 = "931" 
     932 = "932" 
     933 = "933" 
     934 = "934" 
     935 = "935" 
     936 = "936" 
     937 = "937" 
     938 = "938" 
     939 = "939" 
     940 = "940" 
     941 = "941" 
     942 = "942" 
     943 = "943" 
     944 = "944" 
     945 = "945" 
     946 = "946" 
     947 = "947" 
     948 = "948" 
     949 = "949" 
     950 = "950" 
     951 = "951" 
     952 = "952" 
     953 = "953" 
     954 = "954" 
     955 = "955" 
     956 = "956" 
     957 = "957" 
     958 = "958" 
     959 = "959" 
     960 = "960" 
     961 = "961" 
     962 = "962" 
     963 = "963" 
     964 = "964" 
     965 = "965" 
     966 = "966" 
     967 = "967" 
     968 = "968" 
     969 = "969" 
     970 = "970" 
     971 = "971" 
     972 = "972" 
     973 = "973" 
     974 = "974" 
     975 = "975" 
     976 = "976" 
     977 = "977" 
     978 = "978" 
     979 = "979" 
     980 = "980" 
     981 = "981" 
     982 = "982" 
     983 = "983" 
     984 = "984" 
     985 = "985" 
     986 = "986" 
     987 = "987" 
     988 = "988" 
     989 = "989" 
     990 = "990" 
     991 = "991" 
     992 = "992" 
     993 = "993" 
     994 = "994" 
     995 = "995" 
     996 = "996" 
     997 = "997" 
     998 = "998" 
     999 = "(NSP)" 
;

value sdnatpf 
     1 = "Nationalit� fran�aise" 
     2 = "Nationalit� �trang�re" 
     3 = "(NSP)" 
;

value sdpnaisp_detf 
     1 = "ACORES, MADERE" 
     2 = "AFGHANISTAN" 
     3 = "AFRIQUE DU SUD" 
     4 = "ALASKA / ETATS-UNIS D'ALASKA" 
     5 = "ALBANIE" 
     6 = "ALGERIE" 
     7 = "ALLEMAGNE" 
     8 = "ANDORRE" 
     9 = "ANGOLA" 
     10 = "ANGUILLA" 
     11 = "ANTIGUA-ET-BARBUDA" 
     12 = "ANTILLES NEERLANDAISES" 
     13 = "ARABIE SAOUDITE" 
     14 = "ARGENTINE" 
     15 = "ARMENIE" 
     16 = "ARUBA" 
     17 = "AUSTRALIE" 
     18 = "AUTRICHE" 
     19 = "AZERBAIDJAN" 
     20 = "BAHAMAS" 
     21 = "BAHREIN" 
     22 = "BANGLADESH" 
     23 = "BARBADE" 
     24 = "BELGIQUE" 
     25 = "BELIZE" 
     26 = "BENIN" 
     27 = "BERMUDES" 
     28 = "BHOUTAN" 
     29 = "BIELORUSSIE" 
     30 = "BIRMANIE" 
     31 = "BOLIVIE" 
     32 = "BOSNIE-HERZEGOVINE" 
     33 = "BOTSWANA" 
     34 = "BOUVET (ILE)" 
     35 = "BRESIL" 
     36 = "BRUNEI" 
     37 = "BULGARIE" 
     38 = "BURKINA" 
     39 = "BURUNDI" 
     40 = "CAIMANES (ILES)" 
     41 = "CAMBODGE" 
     42 = "CAMEROUN" 
     43 = "CANADA" 
     44 = "CANARIES (ILES)" 
     45 = "CAP-VERT" 
     46 = "CENTRAFRICAINE (REPUBLIQUE)" 
     47 = "CHILI" 
     48 = "CHINE" 
     49 = "CHRISTMAS (ILE)" 
     50 = "CHYPRE" 
     51 = "CLIPPERTON (ILE)" 
     52 = "COCOS ou KEELING (ILES)" 
     53 = "COLOMBIE" 
     54 = "COMORES" 
     55 = "CONGO" 
     56 = "CONGO (REPUBLIQUE DEMOCRATIQUE)" 
     57 = "COOK (ILES)" 
     58 = "COREE" 
     59 = "COREE (REPUBLIQUE DE)" 
     60 = "COREE (REPUBLIQUE POPULAIRE DEMOCRATIQUE DE)" 
     61 = "COSTA RICA" 
     62 = "COTE D'IVOIRE" 
     63 = "CROATIE" 
     64 = "CUBA" 
     65 = "DANEMARK" 
     66 = "DJIBOUTI" 
     67 = "DOMINICAINE (REPUBLIQUE)" 
     68 = "DOMINIQUE" 
     69 = "EGYPTE" 
     70 = "EL SALVADOR" 
     71 = "EMIRATS ARABES UNIS" 
     72 = "EQUATEUR" 
     73 = "ERYTHREE" 
     74 = "ESPAGNE" 
     75 = "ESTONIE" 
     76 = "ETATS MALAIS NON FEDERES" 
     77 = "ETATS-UNIS" 
     78 = "ETHIOPIE" 
     79 = "EX-REPUBLIQUE YOUGOSLAVE DE MACEDOINE / MACEDOINE" 
     80 = "FEROE (ILES)" 
     81 = "FIDJI" 
     82 = "FINLANDE" 
     83 = "FRANCE" 
     84 = "GABON" 
     85 = "GAMBIE" 
     86 = "GEORGIE" 
     87 = "GEORGIE DU SUD ET LES ILES SANDWICH DU SUD" 
     88 = "GHANA" 
     89 = "GIBRALTAR" 
     90 = "GOA" 
     91 = "GRECE" 
     92 = "GRENADE" 
     93 = "GROENLAND" 
     94 = "GUADELOUPE" 
     95 = "GUAM" 
     96 = "GUATEMALA" 
     97 = "GUERNESEY" 
     98 = "GUINEE" 
     99 = "GUINEE EQUATORIALE" 
     100 = "GUINEE-BISSAU" 
     101 = "GUYANA" 
     102 = "GUYANE" 
     103 = "HAITI" 
     104 = "HAWAII (ILES)" 
     105 = "HEARD ET MACDONALD (ILES)" 
     106 = "HONDURAS" 
     107 = "HONG-KONG" 
     108 = "HONGRIE" 
     109 = "ILES PORTUGAISES DE L'OCEAN INDIEN" 
     110 = "INDE" 
     111 = "INDONESIE" 
     112 = "IRAN" 
     113 = "IRAQ" 
     114 = "IRLANDE, ou EIRE" 
     115 = "ISLANDE" 
     116 = "ISRAEL" 
     117 = "ITALIE" 
     118 = "JAMAIQUE" 
     119 = "JAPON" 
     120 = "JERSEY" 
     121 = "JORDANIE" 
     122 = "KAMTCHATKA" 
     123 = "KAZAKHSTAN" 
     124 = "KENYA" 
     125 = "KIRGHIZISTAN" 
     126 = "KIRIBATI" 
     127 = "KOWEIT" 
     128 = "LA REUNION" 
     129 = "LABRADOR" 
     130 = "LAOS" 
     131 = "LESOTHO" 
     132 = "LETTONIE" 
     133 = "LIBAN" 
     134 = "LIBERIA" 
     135 = "LIBYE" 
     136 = "LIECHTENSTEIN" 
     137 = "LITUANIE" 
     138 = "LUXEMBOURG" 
     139 = "MACAO" 
     140 = "MADAGASCAR" 
     141 = "MALAISIE" 
     142 = "MALAWI" 
     143 = "MALDIVES" 
     144 = "MALI" 
     145 = "MALOUINES, OU FALKLAND (ILES)" 
     146 = "MALTE" 
     147 = "MAN (ILE)" 
     148 = "MANDCHOURIE" 
     149 = "MARIANNES DU NORD (ILES)" 
     150 = "MAROC" 
     151 = "MARSHALL (ILES)" 
     152 = "MARTINIQUE" 
     153 = "MAURICE" 
     154 = "MAURITANIE" 
     155 = "MAYOTTE" 
     156 = "MEXIQUE" 
     157 = "MICRONESIE (ETATS FEDERES DE)" 
     158 = "MOLDAVIE" 
     159 = "MONACO" 
     160 = "MONGOLIE" 
     161 = "MONTENEGRO" 
     162 = "MONTSERRAT" 
     163 = "MOZAMBIQUE" 
     164 = "NAMIBIE" 
     165 = "NAURU" 
     166 = "NEPAL" 
     167 = "NICARAGUA" 
     168 = "NIGER" 
     169 = "NIGERIA" 
     170 = "NIUE" 
     171 = "NORFOLK (ILE)" 
     172 = "NORVEGE" 
     173 = "NOUVELLE-CALEDONIE" 
     174 = "NOUVELLE-ZELANDE" 
     175 = "OCEAN INDIEN (TERRITOIRE BRITANNIQUE DE L')" 
     176 = "OMAN" 
     177 = "OUGANDA" 
     178 = "OUZBEKISTAN" 
     179 = "PAKISTAN" 
     180 = "PALAOS (ILES)" 
     181 = "PALESTINE" 
     182 = "PANAMA" 
     183 = "PAPOUASIE-NOUVELLE-GUINEE" 
     184 = "PARAGUAY" 
     185 = "PAYS-BAS" 
     186 = "PEROU" 
     187 = "PHILIPPINES" 
     188 = "PITCAIRN (ILE)" 
     189 = "POLOGNE" 
     190 = "POLYNESIE FRANCAISE" 
     191 = "PORTO RICO" 
     192 = "PORTUGAL" 
     193 = "POSSESSIONS BRITANNIQUES AU PROCHE-ORIENT" 
     194 = "PRESIDES" 
     195 = "PROVINCES ESPAGNOLES D'AFRIQUE" 
     196 = "QATAR" 
     197 = "REPUBLIQUE DEMOCRATIQUE ALLEMANDE" 
     198 = "REPUBLIQUE FEDERALE D'ALLEMAGNE" 
     199 = "ROUMANIE" 
     200 = "ROYAUME-UNI" 
     201 = "RUSSIE / FEDERATION DE RUSSIE" 
     202 = "RWANDA" 
     203 = "SAHARA OCCIDENTAL" 
     204 = "SAINT-BARTHELEMY" 
     205 = "SAINT-CHRISTOPHE-ET-NIEVES" 
     206 = "SAINTE-HELENE" 
     207 = "SAINTE-LUCIE" 
     208 = "SAINT-MARIN" 
     209 = "SAINT-MARTIN" 
     210 = "SAINT-PIERRE-ET-MIQUELON" 
     211 = "SAINT-VINCENT-ET-LES GRENADINES" 
     212 = "SALOMON (ILES)" 
     213 = "SAMOA AMERICAINES" 
     214 = "SAMOA OCCIDENTALES" 
     215 = "SAO TOME-ET-PRINCIPE" 
     216 = "SENEGAL" 
     217 = "SERBIE" 
     218 = "SEYCHELLES" 
     219 = "SIBERIE" 
     220 = "SIERRA LEONE" 
     221 = "SINGAPOUR" 
     222 = "SLOVAQUIE" 
     223 = "SLOVENIE" 
     224 = "SOMALIE" 
     225 = "SOUDAN" 
     226 = "SOUDAN ANGLO-EGYPTIEN, KENYA, OUGANDA" 
     227 = "SRI LANKA" 
     228 = "SUEDE" 
     229 = "SUISSE" 
     230 = "SURINAME" 
     231 = "SVALBARD et ILE JAN MAYEN" 
     232 = "SWAZILAND" 
     233 = "SYRIE" 
     234 = "TADJIKISTAN" 
     235 = "TAIWAN" 
     236 = "TANGER" 
     237 = "TANZANIE" 
     238 = "TCHAD" 
     239 = "TCHECOSLOVAQUIE" 
     240 = "TCHEQUE (REPUBLIQUE)" 
     241 = "TERR. DES ETATS-UNIS D'AMERIQUE EN AMERIQUE" 
     242 = "TERR. DES ETATS-UNIS D'AMERIQUE EN OCEANIE" 
     243 = "TERR. DU ROYAUME-UNI DANS L'ATLANTIQUE SUD" 
     244 = "TERRE-NEUVE" 
     245 = "TERRES AUSTRALES FRANCAISES" 
     246 = "TERRITOIRE DES PAYS-BAS" 
     247 = "TERRITOIRES DU ROYAUME-UNI AUX ANTILLES" 
     248 = "THAILANDE" 
     249 = "TIMOR ORIENTAL" 
     250 = "TOGO" 
     251 = "TOKELAU" 
     252 = "TONGA" 
     253 = "TRINITE-ET-TOBAGO" 
     254 = "TUNISIE" 
     255 = "TURKESTAN RUSSE" 
     256 = "TURKMENISTAN" 
     257 = "TURKS ET CAIQUES (ILES)" 
     258 = "TURQUIE" 
     259 = "TURQUIE D'EUROPE" 
     260 = "TUVALU" 
     261 = "UKRAINE" 
     262 = "URUGUAY" 
     263 = "VANUATU" 
     264 = "VATICAN, ou SAINT-SIEGE" 
     265 = "VENEZUELA" 
     266 = "VIERGES BRITANNIQUES (ILES)" 
     267 = "VIERGES DES ETATS-UNIS (ILES)" 
     268 = "VIET NAM" 
     269 = "VIET NAM DU NORD" 
     270 = "VIET NAM DU SUD" 
     271 = "WALLIS-ET-FUTUNA" 
     272 = "YEMEN" 
     273 = "YEMEN (REPUBLIQUE ARABE DU)" 
     274 = "YEMEN DEMOCRATIQUE" 
     275 = "ZAMBIE" 
     276 = "ZANZIBAR" 
     277 = "ZIMBABWE" 
     278 = "Autre : Pr�ciser" 
     279 = "279" 
     280 = "280" 
     281 = "281" 
     282 = "282" 
     283 = "283" 
     284 = "284" 
     285 = "285" 
     286 = "286" 
     287 = "287" 
     288 = "288" 
     289 = "289" 
     290 = "290" 
     291 = "BELARUS" 
     292 = "HUTT RIVER" 
     293 = "MYANMAR" 
     294 = "SAINT-KITTS-ET-NEVIS" 
     295 = "SAMOA" 
     296 = "SEBORGA" 
     297 = "TIBET" 
     298 = "YOUGOSLAVIE" 
     299 = "299" 
     300 = "300" 
     301 = "301" 
     302 = "302" 
     303 = "303" 
     304 = "304" 
     305 = "305" 
     306 = "306" 
     307 = "307" 
     308 = "308" 
     309 = "309" 
     310 = "310" 
     311 = "311" 
     312 = "312" 
     313 = "313" 
     314 = "314" 
     315 = "315" 
     316 = "316" 
     317 = "317" 
     318 = "318" 
     319 = "319" 
     320 = "320" 
     321 = "321" 
     322 = "322" 
     323 = "323" 
     324 = "324" 
     325 = "325" 
     326 = "326" 
     327 = "327" 
     328 = "328" 
     329 = "329" 
     330 = "330" 
     331 = "331" 
     332 = "332" 
     333 = "333" 
     334 = "334" 
     335 = "335" 
     336 = "336" 
     337 = "337" 
     338 = "338" 
     339 = "339" 
     340 = "340" 
     341 = "341" 
     342 = "342" 
     343 = "343" 
     344 = "344" 
     345 = "345" 
     346 = "346" 
     347 = "347" 
     348 = "348" 
     349 = "349" 
     350 = "350" 
     351 = "351" 
     352 = "352" 
     353 = "353" 
     354 = "354" 
     355 = "355" 
     356 = "356" 
     357 = "357" 
     358 = "358" 
     359 = "359" 
     360 = "360" 
     361 = "361" 
     362 = "362" 
     363 = "363" 
     364 = "364" 
     365 = "365" 
     366 = "366" 
     367 = "367" 
     368 = "368" 
     369 = "369" 
     370 = "370" 
     371 = "371" 
     372 = "372" 
     373 = "373" 
     374 = "374" 
     375 = "375" 
     376 = "376" 
     377 = "377" 
     378 = "378" 
     379 = "379" 
     380 = "380" 
     381 = "381" 
     382 = "382" 
     383 = "383" 
     384 = "384" 
     385 = "385" 
     386 = "386" 
     387 = "387" 
     388 = "388" 
     389 = "389" 
     390 = "390" 
     391 = "391" 
     392 = "392" 
     393 = "393" 
     394 = "394" 
     395 = "395" 
     396 = "396" 
     397 = "397" 
     398 = "398" 
     399 = "399" 
     400 = "400" 
     401 = "401" 
     402 = "402" 
     403 = "403" 
     404 = "404" 
     405 = "405" 
     406 = "406" 
     407 = "407" 
     408 = "408" 
     409 = "409" 
     410 = "410" 
     411 = "411" 
     412 = "412" 
     413 = "413" 
     414 = "414" 
     415 = "415" 
     416 = "416" 
     417 = "417" 
     418 = "418" 
     419 = "419" 
     420 = "420" 
     421 = "421" 
     422 = "422" 
     423 = "423" 
     424 = "424" 
     425 = "425" 
     426 = "426" 
     427 = "427" 
     428 = "428" 
     429 = "429" 
     430 = "430" 
     431 = "431" 
     432 = "432" 
     433 = "433" 
     434 = "434" 
     435 = "435" 
     436 = "436" 
     437 = "437" 
     438 = "438" 
     439 = "439" 
     440 = "440" 
     441 = "441" 
     442 = "442" 
     443 = "443" 
     444 = "444" 
     445 = "445" 
     446 = "446" 
     447 = "447" 
     448 = "448" 
     449 = "449" 
     450 = "450" 
     451 = "451" 
     452 = "452" 
     453 = "453" 
     454 = "454" 
     455 = "455" 
     456 = "456" 
     457 = "457" 
     458 = "458" 
     459 = "459" 
     460 = "460" 
     461 = "461" 
     462 = "462" 
     463 = "463" 
     464 = "464" 
     465 = "465" 
     466 = "466" 
     467 = "467" 
     468 = "468" 
     469 = "469" 
     470 = "470" 
     471 = "471" 
     472 = "472" 
     473 = "473" 
     474 = "474" 
     475 = "475" 
     476 = "476" 
     477 = "477" 
     478 = "478" 
     479 = "479" 
     480 = "480" 
     481 = "481" 
     482 = "482" 
     483 = "483" 
     484 = "484" 
     485 = "485" 
     486 = "486" 
     487 = "487" 
     488 = "488" 
     489 = "489" 
     490 = "490" 
     491 = "491" 
     492 = "492" 
     493 = "493" 
     494 = "494" 
     495 = "495" 
     496 = "496" 
     497 = "497" 
     498 = "498" 
     499 = "499" 
     500 = "500" 
     501 = "501" 
     502 = "502" 
     503 = "503" 
     504 = "504" 
     505 = "505" 
     506 = "506" 
     507 = "507" 
     508 = "508" 
     509 = "509" 
     510 = "510" 
     511 = "511" 
     512 = "512" 
     513 = "513" 
     514 = "514" 
     515 = "515" 
     516 = "516" 
     517 = "517" 
     518 = "518" 
     519 = "519" 
     520 = "520" 
     521 = "521" 
     522 = "522" 
     523 = "523" 
     524 = "524" 
     525 = "525" 
     526 = "526" 
     527 = "527" 
     528 = "528" 
     529 = "529" 
     530 = "530" 
     531 = "531" 
     532 = "532" 
     533 = "533" 
     534 = "534" 
     535 = "535" 
     536 = "536" 
     537 = "537" 
     538 = "538" 
     539 = "539" 
     540 = "540" 
     541 = "541" 
     542 = "542" 
     543 = "543" 
     544 = "544" 
     545 = "545" 
     546 = "546" 
     547 = "547" 
     548 = "548" 
     549 = "549" 
     550 = "550" 
     551 = "551" 
     552 = "552" 
     553 = "553" 
     554 = "554" 
     555 = "555" 
     556 = "556" 
     557 = "557" 
     558 = "558" 
     559 = "559" 
     560 = "560" 
     561 = "561" 
     562 = "562" 
     563 = "563" 
     564 = "564" 
     565 = "565" 
     566 = "566" 
     567 = "567" 
     568 = "568" 
     569 = "569" 
     570 = "570" 
     571 = "571" 
     572 = "572" 
     573 = "573" 
     574 = "574" 
     575 = "575" 
     576 = "576" 
     577 = "577" 
     578 = "578" 
     579 = "579" 
     580 = "580" 
     581 = "581" 
     582 = "582" 
     583 = "583" 
     584 = "584" 
     585 = "585" 
     586 = "586" 
     587 = "587" 
     588 = "588" 
     589 = "589" 
     590 = "590" 
     591 = "591" 
     592 = "592" 
     593 = "593" 
     594 = "594" 
     595 = "595" 
     596 = "596" 
     597 = "597" 
     598 = "598" 
     599 = "599" 
     600 = "600" 
     601 = "601" 
     602 = "602" 
     603 = "603" 
     604 = "604" 
     605 = "605" 
     606 = "606" 
     607 = "607" 
     608 = "608" 
     609 = "609" 
     610 = "610" 
     611 = "611" 
     612 = "612" 
     613 = "613" 
     614 = "614" 
     615 = "615" 
     616 = "616" 
     617 = "617" 
     618 = "618" 
     619 = "619" 
     620 = "620" 
     621 = "621" 
     622 = "622" 
     623 = "623" 
     624 = "624" 
     625 = "625" 
     626 = "626" 
     627 = "627" 
     628 = "628" 
     629 = "629" 
     630 = "630" 
     631 = "631" 
     632 = "632" 
     633 = "633" 
     634 = "634" 
     635 = "635" 
     636 = "636" 
     637 = "637" 
     638 = "638" 
     639 = "639" 
     640 = "640" 
     641 = "641" 
     642 = "642" 
     643 = "643" 
     644 = "644" 
     645 = "645" 
     646 = "646" 
     647 = "647" 
     648 = "648" 
     649 = "649" 
     650 = "650" 
     651 = "651" 
     652 = "652" 
     653 = "653" 
     654 = "654" 
     655 = "655" 
     656 = "656" 
     657 = "657" 
     658 = "658" 
     659 = "659" 
     660 = "660" 
     661 = "661" 
     662 = "662" 
     663 = "663" 
     664 = "664" 
     665 = "665" 
     666 = "666" 
     667 = "667" 
     668 = "668" 
     669 = "669" 
     670 = "670" 
     671 = "671" 
     672 = "672" 
     673 = "673" 
     674 = "674" 
     675 = "675" 
     676 = "676" 
     677 = "677" 
     678 = "678" 
     679 = "679" 
     680 = "680" 
     681 = "681" 
     682 = "682" 
     683 = "683" 
     684 = "684" 
     685 = "685" 
     686 = "686" 
     687 = "687" 
     688 = "688" 
     689 = "689" 
     690 = "690" 
     691 = "691" 
     692 = "692" 
     693 = "693" 
     694 = "694" 
     695 = "695" 
     696 = "696" 
     697 = "697" 
     698 = "698" 
     699 = "699" 
     700 = "700" 
     701 = "701" 
     702 = "702" 
     703 = "703" 
     704 = "704" 
     705 = "705" 
     706 = "706" 
     707 = "707" 
     708 = "708" 
     709 = "709" 
     710 = "710" 
     711 = "711" 
     712 = "712" 
     713 = "713" 
     714 = "714" 
     715 = "715" 
     716 = "716" 
     717 = "717" 
     718 = "718" 
     719 = "719" 
     720 = "720" 
     721 = "721" 
     722 = "722" 
     723 = "723" 
     724 = "724" 
     725 = "725" 
     726 = "726" 
     727 = "727" 
     728 = "728" 
     729 = "729" 
     730 = "730" 
     731 = "731" 
     732 = "732" 
     733 = "733" 
     734 = "734" 
     735 = "735" 
     736 = "736" 
     737 = "737" 
     738 = "738" 
     739 = "739" 
     740 = "740" 
     741 = "741" 
     742 = "742" 
     743 = "743" 
     744 = "744" 
     745 = "745" 
     746 = "746" 
     747 = "747" 
     748 = "748" 
     749 = "749" 
     750 = "750" 
     751 = "751" 
     752 = "752" 
     753 = "753" 
     754 = "754" 
     755 = "755" 
     756 = "756" 
     757 = "757" 
     758 = "758" 
     759 = "759" 
     760 = "760" 
     761 = "761" 
     762 = "762" 
     763 = "763" 
     764 = "764" 
     765 = "765" 
     766 = "766" 
     767 = "767" 
     768 = "768" 
     769 = "769" 
     770 = "770" 
     771 = "771" 
     772 = "772" 
     773 = "773" 
     774 = "774" 
     775 = "775" 
     776 = "776" 
     777 = "777" 
     778 = "778" 
     779 = "779" 
     780 = "780" 
     781 = "781" 
     782 = "782" 
     783 = "783" 
     784 = "784" 
     785 = "785" 
     786 = "786" 
     787 = "787" 
     788 = "788" 
     789 = "789" 
     790 = "790" 
     791 = "791" 
     792 = "792" 
     793 = "793" 
     794 = "794" 
     795 = "795" 
     796 = "796" 
     797 = "797" 
     798 = "798" 
     799 = "799" 
     800 = "800" 
     801 = "801" 
     802 = "802" 
     803 = "803" 
     804 = "804" 
     805 = "805" 
     806 = "806" 
     807 = "807" 
     808 = "808" 
     809 = "809" 
     810 = "810" 
     811 = "811" 
     812 = "812" 
     813 = "813" 
     814 = "814" 
     815 = "815" 
     816 = "816" 
     817 = "817" 
     818 = "818" 
     819 = "819" 
     820 = "820" 
     821 = "821" 
     822 = "822" 
     823 = "823" 
     824 = "824" 
     825 = "825" 
     826 = "826" 
     827 = "827" 
     828 = "828" 
     829 = "829" 
     830 = "830" 
     831 = "831" 
     832 = "832" 
     833 = "833" 
     834 = "834" 
     835 = "835" 
     836 = "836" 
     837 = "837" 
     838 = "838" 
     839 = "839" 
     840 = "840" 
     841 = "841" 
     842 = "842" 
     843 = "843" 
     844 = "844" 
     845 = "845" 
     846 = "846" 
     847 = "847" 
     848 = "848" 
     849 = "849" 
     850 = "850" 
     851 = "851" 
     852 = "852" 
     853 = "853" 
     854 = "854" 
     855 = "855" 
     856 = "856" 
     857 = "857" 
     858 = "858" 
     859 = "859" 
     860 = "860" 
     861 = "861" 
     862 = "862" 
     863 = "863" 
     864 = "864" 
     865 = "865" 
     866 = "866" 
     867 = "867" 
     868 = "868" 
     869 = "869" 
     870 = "870" 
     871 = "871" 
     872 = "872" 
     873 = "873" 
     874 = "874" 
     875 = "875" 
     876 = "876" 
     877 = "877" 
     878 = "878" 
     879 = "879" 
     880 = "880" 
     881 = "881" 
     882 = "882" 
     883 = "883" 
     884 = "884" 
     885 = "885" 
     886 = "886" 
     887 = "887" 
     888 = "888" 
     889 = "889" 
     890 = "890" 
     891 = "891" 
     892 = "892" 
     893 = "893" 
     894 = "894" 
     895 = "895" 
     896 = "896" 
     897 = "897" 
     898 = "898" 
     899 = "899" 
     900 = "900" 
     901 = "901" 
     902 = "902" 
     903 = "903" 
     904 = "904" 
     905 = "905" 
     906 = "906" 
     907 = "907" 
     908 = "908" 
     909 = "909" 
     910 = "910" 
     911 = "911" 
     912 = "912" 
     913 = "913" 
     914 = "914" 
     915 = "915" 
     916 = "916" 
     917 = "917" 
     918 = "918" 
     919 = "919" 
     920 = "920" 
     921 = "921" 
     922 = "922" 
     923 = "923" 
     924 = "924" 
     925 = "925" 
     926 = "926" 
     927 = "927" 
     928 = "928" 
     929 = "929" 
     930 = "930" 
     931 = "931" 
     932 = "932" 
     933 = "933" 
     934 = "934" 
     935 = "935" 
     936 = "936" 
     937 = "937" 
     938 = "938" 
     939 = "939" 
     940 = "940" 
     941 = "941" 
     942 = "942" 
     943 = "943" 
     944 = "944" 
     945 = "945" 
     946 = "946" 
     947 = "947" 
     948 = "948" 
     949 = "949" 
     950 = "950" 
     951 = "951" 
     952 = "952" 
     953 = "953" 
     954 = "954" 
     955 = "955" 
     956 = "956" 
     957 = "957" 
     958 = "958" 
     959 = "959" 
     960 = "960" 
     961 = "961" 
     962 = "962" 
     963 = "963" 
     964 = "964" 
     965 = "965" 
     966 = "966" 
     967 = "967" 
     968 = "968" 
     969 = "969" 
     970 = "970" 
     971 = "971" 
     972 = "972" 
     973 = "973" 
     974 = "974" 
     975 = "975" 
     976 = "976" 
     977 = "977" 
     978 = "978" 
     979 = "979" 
     980 = "980" 
     981 = "981" 
     982 = "982" 
     983 = "983" 
     984 = "984" 
     985 = "985" 
     986 = "986" 
     987 = "987" 
     988 = "988" 
     989 = "989" 
     990 = "990" 
     991 = "991" 
     992 = "992" 
     993 = "993" 
     994 = "994" 
     995 = "995" 
     996 = "996" 
     997 = "997" 
     998 = "998" 
     999 = "(NSP)" 
;

value sdnatmf 
     1 = "Nationalit� fran�aise" 
     2 = "Nationalit� �trang�re" 
     3 = "(NSP)" 
;

value sdpnaism_detf 
     1 = "ACORES, MADERE" 
     2 = "AFGHANISTAN" 
     3 = "AFRIQUE DU SUD" 
     4 = "ALASKA / ETATS-UNIS D'ALASKA" 
     5 = "ALBANIE" 
     6 = "ALGERIE" 
     7 = "ALLEMAGNE" 
     8 = "ANDORRE" 
     9 = "ANGOLA" 
     10 = "ANGUILLA" 
     11 = "ANTIGUA-ET-BARBUDA" 
     12 = "ANTILLES NEERLANDAISES" 
     13 = "ARABIE SAOUDITE" 
     14 = "ARGENTINE" 
     15 = "ARMENIE" 
     16 = "ARUBA" 
     17 = "AUSTRALIE" 
     18 = "AUTRICHE" 
     19 = "AZERBAIDJAN" 
     20 = "BAHAMAS" 
     21 = "BAHREIN" 
     22 = "BANGLADESH" 
     23 = "BARBADE" 
     24 = "BELGIQUE" 
     25 = "BELIZE" 
     26 = "BENIN" 
     27 = "BERMUDES" 
     28 = "BHOUTAN" 
     29 = "BIELORUSSIE" 
     30 = "BIRMANIE" 
     31 = "BOLIVIE" 
     32 = "BOSNIE-HERZEGOVINE" 
     33 = "BOTSWANA" 
     34 = "BOUVET (ILE)" 
     35 = "BRESIL" 
     36 = "BRUNEI" 
     37 = "BULGARIE" 
     38 = "BURKINA" 
     39 = "BURUNDI" 
     40 = "CAIMANES (ILES)" 
     41 = "CAMBODGE" 
     42 = "CAMEROUN" 
     43 = "CANADA" 
     44 = "CANARIES (ILES)" 
     45 = "CAP-VERT" 
     46 = "CENTRAFRICAINE (REPUBLIQUE)" 
     47 = "CHILI" 
     48 = "CHINE" 
     49 = "CHRISTMAS (ILE)" 
     50 = "CHYPRE" 
     51 = "CLIPPERTON (ILE)" 
     52 = "COCOS ou KEELING (ILES)" 
     53 = "COLOMBIE" 
     54 = "COMORES" 
     55 = "CONGO" 
     56 = "CONGO (REPUBLIQUE DEMOCRATIQUE)" 
     57 = "COOK (ILES)" 
     58 = "COREE" 
     59 = "COREE (REPUBLIQUE DE)" 
     60 = "COREE (REPUBLIQUE POPULAIRE DEMOCRATIQUE DE)" 
     61 = "COSTA RICA" 
     62 = "COTE D'IVOIRE" 
     63 = "CROATIE" 
     64 = "CUBA" 
     65 = "DANEMARK" 
     66 = "DJIBOUTI" 
     67 = "DOMINICAINE (REPUBLIQUE)" 
     68 = "DOMINIQUE" 
     69 = "EGYPTE" 
     70 = "EL SALVADOR" 
     71 = "EMIRATS ARABES UNIS" 
     72 = "EQUATEUR" 
     73 = "ERYTHREE" 
     74 = "ESPAGNE" 
     75 = "ESTONIE" 
     76 = "ETATS MALAIS NON FEDERES" 
     77 = "ETATS-UNIS" 
     78 = "ETHIOPIE" 
     79 = "EX-REPUBLIQUE YOUGOSLAVE DE MACEDOINE / MACEDOINE" 
     80 = "FEROE (ILES)" 
     81 = "FIDJI" 
     82 = "FINLANDE" 
     83 = "FRANCE" 
     84 = "GABON" 
     85 = "GAMBIE" 
     86 = "GEORGIE" 
     87 = "GEORGIE DU SUD ET LES ILES SANDWICH DU SUD" 
     88 = "GHANA" 
     89 = "GIBRALTAR" 
     90 = "GOA" 
     91 = "GRECE" 
     92 = "GRENADE" 
     93 = "GROENLAND" 
     94 = "GUADELOUPE" 
     95 = "GUAM" 
     96 = "GUATEMALA" 
     97 = "GUERNESEY" 
     98 = "GUINEE" 
     99 = "GUINEE EQUATORIALE" 
     100 = "GUINEE-BISSAU" 
     101 = "GUYANA" 
     102 = "GUYANE" 
     103 = "HAITI" 
     104 = "HAWAII (ILES)" 
     105 = "HEARD ET MACDONALD (ILES)" 
     106 = "HONDURAS" 
     107 = "HONG-KONG" 
     108 = "HONGRIE" 
     109 = "ILES PORTUGAISES DE L'OCEAN INDIEN" 
     110 = "INDE" 
     111 = "INDONESIE" 
     112 = "IRAN" 
     113 = "IRAQ" 
     114 = "IRLANDE, ou EIRE" 
     115 = "ISLANDE" 
     116 = "ISRAEL" 
     117 = "ITALIE" 
     118 = "JAMAIQUE" 
     119 = "JAPON" 
     120 = "JERSEY" 
     121 = "JORDANIE" 
     122 = "KAMTCHATKA" 
     123 = "KAZAKHSTAN" 
     124 = "KENYA" 
     125 = "KIRGHIZISTAN" 
     126 = "KIRIBATI" 
     127 = "KOWEIT" 
     128 = "LA REUNION" 
     129 = "LABRADOR" 
     130 = "LAOS" 
     131 = "LESOTHO" 
     132 = "LETTONIE" 
     133 = "LIBAN" 
     134 = "LIBERIA" 
     135 = "LIBYE" 
     136 = "LIECHTENSTEIN" 
     137 = "LITUANIE" 
     138 = "LUXEMBOURG" 
     139 = "MACAO" 
     140 = "MADAGASCAR" 
     141 = "MALAISIE" 
     142 = "MALAWI" 
     143 = "MALDIVES" 
     144 = "MALI" 
     145 = "MALOUINES, OU FALKLAND (ILES)" 
     146 = "MALTE" 
     147 = "MAN (ILE)" 
     148 = "MANDCHOURIE" 
     149 = "MARIANNES DU NORD (ILES)" 
     150 = "MAROC" 
     151 = "MARSHALL (ILES)" 
     152 = "MARTINIQUE" 
     153 = "MAURICE" 
     154 = "MAURITANIE" 
     155 = "MAYOTTE" 
     156 = "MEXIQUE" 
     157 = "MICRONESIE (ETATS FEDERES DE)" 
     158 = "MOLDAVIE" 
     159 = "MONACO" 
     160 = "MONGOLIE" 
     161 = "MONTENEGRO" 
     162 = "MONTSERRAT" 
     163 = "MOZAMBIQUE" 
     164 = "NAMIBIE" 
     165 = "NAURU" 
     166 = "NEPAL" 
     167 = "NICARAGUA" 
     168 = "NIGER" 
     169 = "NIGERIA" 
     170 = "NIUE" 
     171 = "NORFOLK (ILE)" 
     172 = "NORVEGE" 
     173 = "NOUVELLE-CALEDONIE" 
     174 = "NOUVELLE-ZELANDE" 
     175 = "OCEAN INDIEN (TERRITOIRE BRITANNIQUE DE L')" 
     176 = "OMAN" 
     177 = "OUGANDA" 
     178 = "OUZBEKISTAN" 
     179 = "PAKISTAN" 
     180 = "PALAOS (ILES)" 
     181 = "PALESTINE" 
     182 = "PANAMA" 
     183 = "PAPOUASIE-NOUVELLE-GUINEE" 
     184 = "PARAGUAY" 
     185 = "PAYS-BAS" 
     186 = "PEROU" 
     187 = "PHILIPPINES" 
     188 = "PITCAIRN (ILE)" 
     189 = "POLOGNE" 
     190 = "POLYNESIE FRANCAISE" 
     191 = "PORTO RICO" 
     192 = "PORTUGAL" 
     193 = "POSSESSIONS BRITANNIQUES AU PROCHE-ORIENT" 
     194 = "PRESIDES" 
     195 = "PROVINCES ESPAGNOLES D'AFRIQUE" 
     196 = "QATAR" 
     197 = "REPUBLIQUE DEMOCRATIQUE ALLEMANDE" 
     198 = "REPUBLIQUE FEDERALE D'ALLEMAGNE" 
     199 = "ROUMANIE" 
     200 = "ROYAUME-UNI" 
     201 = "RUSSIE / FEDERATION DE RUSSIE" 
     202 = "RWANDA" 
     203 = "SAHARA OCCIDENTAL" 
     204 = "SAINT-BARTHELEMY" 
     205 = "SAINT-CHRISTOPHE-ET-NIEVES" 
     206 = "SAINTE-HELENE" 
     207 = "SAINTE-LUCIE" 
     208 = "SAINT-MARIN" 
     209 = "SAINT-MARTIN" 
     210 = "SAINT-PIERRE-ET-MIQUELON" 
     211 = "SAINT-VINCENT-ET-LES GRENADINES" 
     212 = "SALOMON (ILES)" 
     213 = "SAMOA AMERICAINES" 
     214 = "SAMOA OCCIDENTALES" 
     215 = "SAO TOME-ET-PRINCIPE" 
     216 = "SENEGAL" 
     217 = "SERBIE" 
     218 = "SEYCHELLES" 
     219 = "SIBERIE" 
     220 = "SIERRA LEONE" 
     221 = "SINGAPOUR" 
     222 = "SLOVAQUIE" 
     223 = "SLOVENIE" 
     224 = "SOMALIE" 
     225 = "SOUDAN" 
     226 = "SOUDAN ANGLO-EGYPTIEN, KENYA, OUGANDA" 
     227 = "SRI LANKA" 
     228 = "SUEDE" 
     229 = "SUISSE" 
     230 = "SURINAME" 
     231 = "SVALBARD et ILE JAN MAYEN" 
     232 = "SWAZILAND" 
     233 = "SYRIE" 
     234 = "TADJIKISTAN" 
     235 = "TAIWAN" 
     236 = "TANGER" 
     237 = "TANZANIE" 
     238 = "TCHAD" 
     239 = "TCHECOSLOVAQUIE" 
     240 = "TCHEQUE (REPUBLIQUE)" 
     241 = "TERR. DES ETATS-UNIS D'AMERIQUE EN AMERIQUE" 
     242 = "TERR. DES ETATS-UNIS D'AMERIQUE EN OCEANIE" 
     243 = "TERR. DU ROYAUME-UNI DANS L'ATLANTIQUE SUD" 
     244 = "TERRE-NEUVE" 
     245 = "TERRES AUSTRALES FRANCAISES" 
     246 = "TERRITOIRE DES PAYS-BAS" 
     247 = "TERRITOIRES DU ROYAUME-UNI AUX ANTILLES" 
     248 = "THAILANDE" 
     249 = "TIMOR ORIENTAL" 
     250 = "TOGO" 
     251 = "TOKELAU" 
     252 = "TONGA" 
     253 = "TRINITE-ET-TOBAGO" 
     254 = "TUNISIE" 
     255 = "TURKESTAN RUSSE" 
     256 = "TURKMENISTAN" 
     257 = "TURKS ET CAIQUES (ILES)" 
     258 = "TURQUIE" 
     259 = "TURQUIE D'EUROPE" 
     260 = "TUVALU" 
     261 = "UKRAINE" 
     262 = "URUGUAY" 
     263 = "VANUATU" 
     264 = "VATICAN, ou SAINT-SIEGE" 
     265 = "VENEZUELA" 
     266 = "VIERGES BRITANNIQUES (ILES)" 
     267 = "VIERGES DES ETATS-UNIS (ILES)" 
     268 = "VIET NAM" 
     269 = "VIET NAM DU NORD" 
     270 = "VIET NAM DU SUD" 
     271 = "WALLIS-ET-FUTUNA" 
     272 = "YEMEN" 
     273 = "YEMEN (REPUBLIQUE ARABE DU)" 
     274 = "YEMEN DEMOCRATIQUE" 
     275 = "ZAMBIE" 
     276 = "ZANZIBAR" 
     277 = "ZIMBABWE" 
     278 = "Autre : Pr�ciser" 
     279 = "279" 
     280 = "280" 
     281 = "281" 
     282 = "282" 
     283 = "283" 
     284 = "284" 
     285 = "285" 
     286 = "286" 
     287 = "287" 
     288 = "288" 
     289 = "289" 
     290 = "290" 
     291 = "BELARUS" 
     292 = "HUTT RIVER" 
     293 = "MYANMAR" 
     294 = "SAINT-KITTS-ET-NEVIS" 
     295 = "SAMOA" 
     296 = "SEBORGA" 
     297 = "TIBET" 
     298 = "YOUGOSLAVIE" 
     299 = "299" 
     300 = "300" 
     301 = "301" 
     302 = "302" 
     303 = "303" 
     304 = "304" 
     305 = "305" 
     306 = "306" 
     307 = "307" 
     308 = "308" 
     309 = "309" 
     310 = "310" 
     311 = "311" 
     312 = "312" 
     313 = "313" 
     314 = "314" 
     315 = "315" 
     316 = "316" 
     317 = "317" 
     318 = "318" 
     319 = "319" 
     320 = "320" 
     321 = "321" 
     322 = "322" 
     323 = "323" 
     324 = "324" 
     325 = "325" 
     326 = "326" 
     327 = "327" 
     328 = "328" 
     329 = "329" 
     330 = "330" 
     331 = "331" 
     332 = "332" 
     333 = "333" 
     334 = "334" 
     335 = "335" 
     336 = "336" 
     337 = "337" 
     338 = "338" 
     339 = "339" 
     340 = "340" 
     341 = "341" 
     342 = "342" 
     343 = "343" 
     344 = "344" 
     345 = "345" 
     346 = "346" 
     347 = "347" 
     348 = "348" 
     349 = "349" 
     350 = "350" 
     351 = "351" 
     352 = "352" 
     353 = "353" 
     354 = "354" 
     355 = "355" 
     356 = "356" 
     357 = "357" 
     358 = "358" 
     359 = "359" 
     360 = "360" 
     361 = "361" 
     362 = "362" 
     363 = "363" 
     364 = "364" 
     365 = "365" 
     366 = "366" 
     367 = "367" 
     368 = "368" 
     369 = "369" 
     370 = "370" 
     371 = "371" 
     372 = "372" 
     373 = "373" 
     374 = "374" 
     375 = "375" 
     376 = "376" 
     377 = "377" 
     378 = "378" 
     379 = "379" 
     380 = "380" 
     381 = "381" 
     382 = "382" 
     383 = "383" 
     384 = "384" 
     385 = "385" 
     386 = "386" 
     387 = "387" 
     388 = "388" 
     389 = "389" 
     390 = "390" 
     391 = "391" 
     392 = "392" 
     393 = "393" 
     394 = "394" 
     395 = "395" 
     396 = "396" 
     397 = "397" 
     398 = "398" 
     399 = "399" 
     400 = "400" 
     401 = "401" 
     402 = "402" 
     403 = "403" 
     404 = "404" 
     405 = "405" 
     406 = "406" 
     407 = "407" 
     408 = "408" 
     409 = "409" 
     410 = "410" 
     411 = "411" 
     412 = "412" 
     413 = "413" 
     414 = "414" 
     415 = "415" 
     416 = "416" 
     417 = "417" 
     418 = "418" 
     419 = "419" 
     420 = "420" 
     421 = "421" 
     422 = "422" 
     423 = "423" 
     424 = "424" 
     425 = "425" 
     426 = "426" 
     427 = "427" 
     428 = "428" 
     429 = "429" 
     430 = "430" 
     431 = "431" 
     432 = "432" 
     433 = "433" 
     434 = "434" 
     435 = "435" 
     436 = "436" 
     437 = "437" 
     438 = "438" 
     439 = "439" 
     440 = "440" 
     441 = "441" 
     442 = "442" 
     443 = "443" 
     444 = "444" 
     445 = "445" 
     446 = "446" 
     447 = "447" 
     448 = "448" 
     449 = "449" 
     450 = "450" 
     451 = "451" 
     452 = "452" 
     453 = "453" 
     454 = "454" 
     455 = "455" 
     456 = "456" 
     457 = "457" 
     458 = "458" 
     459 = "459" 
     460 = "460" 
     461 = "461" 
     462 = "462" 
     463 = "463" 
     464 = "464" 
     465 = "465" 
     466 = "466" 
     467 = "467" 
     468 = "468" 
     469 = "469" 
     470 = "470" 
     471 = "471" 
     472 = "472" 
     473 = "473" 
     474 = "474" 
     475 = "475" 
     476 = "476" 
     477 = "477" 
     478 = "478" 
     479 = "479" 
     480 = "480" 
     481 = "481" 
     482 = "482" 
     483 = "483" 
     484 = "484" 
     485 = "485" 
     486 = "486" 
     487 = "487" 
     488 = "488" 
     489 = "489" 
     490 = "490" 
     491 = "491" 
     492 = "492" 
     493 = "493" 
     494 = "494" 
     495 = "495" 
     496 = "496" 
     497 = "497" 
     498 = "498" 
     499 = "499" 
     500 = "500" 
     501 = "501" 
     502 = "502" 
     503 = "503" 
     504 = "504" 
     505 = "505" 
     506 = "506" 
     507 = "507" 
     508 = "508" 
     509 = "509" 
     510 = "510" 
     511 = "511" 
     512 = "512" 
     513 = "513" 
     514 = "514" 
     515 = "515" 
     516 = "516" 
     517 = "517" 
     518 = "518" 
     519 = "519" 
     520 = "520" 
     521 = "521" 
     522 = "522" 
     523 = "523" 
     524 = "524" 
     525 = "525" 
     526 = "526" 
     527 = "527" 
     528 = "528" 
     529 = "529" 
     530 = "530" 
     531 = "531" 
     532 = "532" 
     533 = "533" 
     534 = "534" 
     535 = "535" 
     536 = "536" 
     537 = "537" 
     538 = "538" 
     539 = "539" 
     540 = "540" 
     541 = "541" 
     542 = "542" 
     543 = "543" 
     544 = "544" 
     545 = "545" 
     546 = "546" 
     547 = "547" 
     548 = "548" 
     549 = "549" 
     550 = "550" 
     551 = "551" 
     552 = "552" 
     553 = "553" 
     554 = "554" 
     555 = "555" 
     556 = "556" 
     557 = "557" 
     558 = "558" 
     559 = "559" 
     560 = "560" 
     561 = "561" 
     562 = "562" 
     563 = "563" 
     564 = "564" 
     565 = "565" 
     566 = "566" 
     567 = "567" 
     568 = "568" 
     569 = "569" 
     570 = "570" 
     571 = "571" 
     572 = "572" 
     573 = "573" 
     574 = "574" 
     575 = "575" 
     576 = "576" 
     577 = "577" 
     578 = "578" 
     579 = "579" 
     580 = "580" 
     581 = "581" 
     582 = "582" 
     583 = "583" 
     584 = "584" 
     585 = "585" 
     586 = "586" 
     587 = "587" 
     588 = "588" 
     589 = "589" 
     590 = "590" 
     591 = "591" 
     592 = "592" 
     593 = "593" 
     594 = "594" 
     595 = "595" 
     596 = "596" 
     597 = "597" 
     598 = "598" 
     599 = "599" 
     600 = "600" 
     601 = "601" 
     602 = "602" 
     603 = "603" 
     604 = "604" 
     605 = "605" 
     606 = "606" 
     607 = "607" 
     608 = "608" 
     609 = "609" 
     610 = "610" 
     611 = "611" 
     612 = "612" 
     613 = "613" 
     614 = "614" 
     615 = "615" 
     616 = "616" 
     617 = "617" 
     618 = "618" 
     619 = "619" 
     620 = "620" 
     621 = "621" 
     622 = "622" 
     623 = "623" 
     624 = "624" 
     625 = "625" 
     626 = "626" 
     627 = "627" 
     628 = "628" 
     629 = "629" 
     630 = "630" 
     631 = "631" 
     632 = "632" 
     633 = "633" 
     634 = "634" 
     635 = "635" 
     636 = "636" 
     637 = "637" 
     638 = "638" 
     639 = "639" 
     640 = "640" 
     641 = "641" 
     642 = "642" 
     643 = "643" 
     644 = "644" 
     645 = "645" 
     646 = "646" 
     647 = "647" 
     648 = "648" 
     649 = "649" 
     650 = "650" 
     651 = "651" 
     652 = "652" 
     653 = "653" 
     654 = "654" 
     655 = "655" 
     656 = "656" 
     657 = "657" 
     658 = "658" 
     659 = "659" 
     660 = "660" 
     661 = "661" 
     662 = "662" 
     663 = "663" 
     664 = "664" 
     665 = "665" 
     666 = "666" 
     667 = "667" 
     668 = "668" 
     669 = "669" 
     670 = "670" 
     671 = "671" 
     672 = "672" 
     673 = "673" 
     674 = "674" 
     675 = "675" 
     676 = "676" 
     677 = "677" 
     678 = "678" 
     679 = "679" 
     680 = "680" 
     681 = "681" 
     682 = "682" 
     683 = "683" 
     684 = "684" 
     685 = "685" 
     686 = "686" 
     687 = "687" 
     688 = "688" 
     689 = "689" 
     690 = "690" 
     691 = "691" 
     692 = "692" 
     693 = "693" 
     694 = "694" 
     695 = "695" 
     696 = "696" 
     697 = "697" 
     698 = "698" 
     699 = "699" 
     700 = "700" 
     701 = "701" 
     702 = "702" 
     703 = "703" 
     704 = "704" 
     705 = "705" 
     706 = "706" 
     707 = "707" 
     708 = "708" 
     709 = "709" 
     710 = "710" 
     711 = "711" 
     712 = "712" 
     713 = "713" 
     714 = "714" 
     715 = "715" 
     716 = "716" 
     717 = "717" 
     718 = "718" 
     719 = "719" 
     720 = "720" 
     721 = "721" 
     722 = "722" 
     723 = "723" 
     724 = "724" 
     725 = "725" 
     726 = "726" 
     727 = "727" 
     728 = "728" 
     729 = "729" 
     730 = "730" 
     731 = "731" 
     732 = "732" 
     733 = "733" 
     734 = "734" 
     735 = "735" 
     736 = "736" 
     737 = "737" 
     738 = "738" 
     739 = "739" 
     740 = "740" 
     741 = "741" 
     742 = "742" 
     743 = "743" 
     744 = "744" 
     745 = "745" 
     746 = "746" 
     747 = "747" 
     748 = "748" 
     749 = "749" 
     750 = "750" 
     751 = "751" 
     752 = "752" 
     753 = "753" 
     754 = "754" 
     755 = "755" 
     756 = "756" 
     757 = "757" 
     758 = "758" 
     759 = "759" 
     760 = "760" 
     761 = "761" 
     762 = "762" 
     763 = "763" 
     764 = "764" 
     765 = "765" 
     766 = "766" 
     767 = "767" 
     768 = "768" 
     769 = "769" 
     770 = "770" 
     771 = "771" 
     772 = "772" 
     773 = "773" 
     774 = "774" 
     775 = "775" 
     776 = "776" 
     777 = "777" 
     778 = "778" 
     779 = "779" 
     780 = "780" 
     781 = "781" 
     782 = "782" 
     783 = "783" 
     784 = "784" 
     785 = "785" 
     786 = "786" 
     787 = "787" 
     788 = "788" 
     789 = "789" 
     790 = "790" 
     791 = "791" 
     792 = "792" 
     793 = "793" 
     794 = "794" 
     795 = "795" 
     796 = "796" 
     797 = "797" 
     798 = "798" 
     799 = "799" 
     800 = "800" 
     801 = "801" 
     802 = "802" 
     803 = "803" 
     804 = "804" 
     805 = "805" 
     806 = "806" 
     807 = "807" 
     808 = "808" 
     809 = "809" 
     810 = "810" 
     811 = "811" 
     812 = "812" 
     813 = "813" 
     814 = "814" 
     815 = "815" 
     816 = "816" 
     817 = "817" 
     818 = "818" 
     819 = "819" 
     820 = "820" 
     821 = "821" 
     822 = "822" 
     823 = "823" 
     824 = "824" 
     825 = "825" 
     826 = "826" 
     827 = "827" 
     828 = "828" 
     829 = "829" 
     830 = "830" 
     831 = "831" 
     832 = "832" 
     833 = "833" 
     834 = "834" 
     835 = "835" 
     836 = "836" 
     837 = "837" 
     838 = "838" 
     839 = "839" 
     840 = "840" 
     841 = "841" 
     842 = "842" 
     843 = "843" 
     844 = "844" 
     845 = "845" 
     846 = "846" 
     847 = "847" 
     848 = "848" 
     849 = "849" 
     850 = "850" 
     851 = "851" 
     852 = "852" 
     853 = "853" 
     854 = "854" 
     855 = "855" 
     856 = "856" 
     857 = "857" 
     858 = "858" 
     859 = "859" 
     860 = "860" 
     861 = "861" 
     862 = "862" 
     863 = "863" 
     864 = "864" 
     865 = "865" 
     866 = "866" 
     867 = "867" 
     868 = "868" 
     869 = "869" 
     870 = "870" 
     871 = "871" 
     872 = "872" 
     873 = "873" 
     874 = "874" 
     875 = "875" 
     876 = "876" 
     877 = "877" 
     878 = "878" 
     879 = "879" 
     880 = "880" 
     881 = "881" 
     882 = "882" 
     883 = "883" 
     884 = "884" 
     885 = "885" 
     886 = "886" 
     887 = "887" 
     888 = "888" 
     889 = "889" 
     890 = "890" 
     891 = "891" 
     892 = "892" 
     893 = "893" 
     894 = "894" 
     895 = "895" 
     896 = "896" 
     897 = "897" 
     898 = "898" 
     899 = "899" 
     900 = "900" 
     901 = "901" 
     902 = "902" 
     903 = "903" 
     904 = "904" 
     905 = "905" 
     906 = "906" 
     907 = "907" 
     908 = "908" 
     909 = "909" 
     910 = "910" 
     911 = "911" 
     912 = "912" 
     913 = "913" 
     914 = "914" 
     915 = "915" 
     916 = "916" 
     917 = "917" 
     918 = "918" 
     919 = "919" 
     920 = "920" 
     921 = "921" 
     922 = "922" 
     923 = "923" 
     924 = "924" 
     925 = "925" 
     926 = "926" 
     927 = "927" 
     928 = "928" 
     929 = "929" 
     930 = "930" 
     931 = "931" 
     932 = "932" 
     933 = "933" 
     934 = "934" 
     935 = "935" 
     936 = "936" 
     937 = "937" 
     938 = "938" 
     939 = "939" 
     940 = "940" 
     941 = "941" 
     942 = "942" 
     943 = "943" 
     944 = "944" 
     945 = "945" 
     946 = "946" 
     947 = "947" 
     948 = "948" 
     949 = "949" 
     950 = "950" 
     951 = "951" 
     952 = "952" 
     953 = "953" 
     954 = "954" 
     955 = "955" 
     956 = "956" 
     957 = "957" 
     958 = "958" 
     959 = "959" 
     960 = "960" 
     961 = "961" 
     962 = "962" 
     963 = "963" 
     964 = "964" 
     965 = "965" 
     966 = "966" 
     967 = "967" 
     968 = "968" 
     969 = "969" 
     970 = "970" 
     971 = "971" 
     972 = "972" 
     973 = "973" 
     974 = "974" 
     975 = "975" 
     976 = "976" 
     977 = "977" 
     978 = "978" 
     979 = "979" 
     980 = "980" 
     981 = "981" 
     982 = "982" 
     983 = "983" 
     984 = "984" 
     985 = "985" 
     986 = "986" 
     987 = "987" 
     988 = "988" 
     989 = "989" 
     990 = "990" 
     991 = "991" 
     992 = "992" 
     993 = "993" 
     994 = "994" 
     995 = "995" 
     996 = "996" 
     997 = "997" 
     998 = "998" 
     999 = "(NSP)" 
;

value natparf 
     1 = "Deux parents fran�ais � la naissance" 
     2 = "Un parent �tranger" 
     3 = "Deux parents �trangers" 
;

value sdnivief 
     1 = "1er quintile de niveau de vie" 
     2 = "2�me quintile de niveau de vie" 
     3 = "3�me quintile de niveau de vie" 
     4 = "4�me quintile de niveau de vie" 
     5 = "5�me quintile de niveau de vie" 
;

DATA  barometre_2000_2017_diff ;
set lib.barometre_2000_2017_diff;
FORMAT sdsexe sdsexef. ;
FORMAT habitat habitatf. ;
FORMAT sdagetr sdagetrf. ;
FORMAT sdsitua sdsituaf. ;
FORMAT sdact sdactf. ;
FORMAT sdstat sdstatf. ;
FORMAT sdpcs7 sdpcs7f. ;
FORMAT sdpcs10 sdpcs10f. ;
FORMAT sdstatemp sdstatempf. ;
FORMAT sdsitfam sdsitfamf. ;
FORMAT sdpr sdprf. ;
FORMAT region regionf. ;
FORMAT sdprsitua sdprsituaf. ;
FORMAT sdpract sdpractf. ;
FORMAT sdprstat sdprstatf. ;
FORMAT sdprpcs7 sdprpcs7f. ;
FORMAT sdprpcs10 sdprpcs10f. ;
FORMAT sdnbpers sdnbpersf. ;
FORMAT sdcouple sdcouplef. ;
FORMAT sdmatri sdmatrif. ;
FORMAT sdmatri_nw sdmatri_nwf. ;
FORMAT sdnbadu sdnbaduf. ;
FORMAT sdsplit sdsplitf. ;
FORMAT sdmut sdmutf. ;
FORMAT og1 og1f. ;
FORMAT og2_ab og2_abf. ;
FORMAT og2_cd og2_cdf. ;
FORMAT og3_1 og3_1f. ;
FORMAT og3_2 og3_2f. ;
FORMAT og4_1 og4_1f. ;
FORMAT og4_2 og4_2f. ;
FORMAT og4_3 og4_3f. ;
FORMAT og4_4 og4_4f. ;
FORMAT og4_5 og4_5f. ;
FORMAT og4_6 og4_6f. ;
FORMAT og4_7 og4_7f. ;
FORMAT og4_8 og4_8f. ;
FORMAT og4_9 og4_9f. ;
FORMAT og5_1 og5_1f. ;
FORMAT og5_2 og5_2f. ;
FORMAT og5_3 og5_3f. ;
FORMAT og5_4 og5_4f. ;
FORMAT og5_5 og5_5f. ;
FORMAT og5_6 og5_6f. ;
FORMAT og5_7 og5_7f. ;
FORMAT og6 og6f. ;
FORMAT og7 og7f. ;
FORMAT og9_ab og9_abf. ;
FORMAT og8_1 og8_1f. ;
FORMAT og8_2 og8_2f. ;
FORMAT og8_4 og8_4f. ;
FORMAT og8_8 og8_8f. ;
FORMAT og8_9 og8_9f. ;
FORMAT og8_10 og8_10f. ;
FORMAT og8_11 og8_11f. ;
FORMAT og9_cd og9_cdf. ;
FORMAT in1 in1f. ;
FORMAT in2 in2f. ;
FORMAT in3 in3f. ;
FORMAT in4_ab in4_abf. ;
FORMAT in4_cd in4_cdf. ;
FORMAT in5_ab in5_abf. ;
FORMAT in5_cd in5_cdf. ;
FORMAT in6 in6f. ;
FORMAT pe1 pe1f. ;
FORMAT pe2 pe2f. ;
FORMAT pe3 pe3f. ;
FORMAT pe9 pe9f. ;
FORMAT pe10 pe10f. ;
FORMAT pe12 pe12f. ;
FORMAT lo1 lo1f. ;
FORMAT lo2 lo2f. ;
FORMAT lo3 lo3f. ;
FORMAT lo4 lo4f. ;
FORMAT lo9 lo9f. ;
FORMAT re1 re1f. ;
FORMAT re2 re2f. ;
FORMAT re3 re3f. ;
FORMAT re7 re7f. ;
FORMAT re8 re8f. ;
FORMAT re9bis re9bisf. ;
FORMAT re9 re9f. ;
FORMAT re10 re10f. ;
FORMAT re11 re11f. ;
FORMAT re12 re12f. ;
FORMAT re13 re13f. ;
FORMAT re14 re14f. ;
FORMAT re15 re15f. ;
FORMAT fa1 fa1f. ;
FORMAT ha1 ha1f. ;
FORMAT ha2 ha2f. ;
FORMAT ha3 ha3f. ;
FORMAT ha4 ha4f. ;
FORMAT ha5_1 ha5_1f. ;
FORMAT ha5_2 ha5_2f. ;
FORMAT ha5_3 ha5_3f. ;
FORMAT ha5_4 ha5_4f. ;
FORMAT de1 de1f. ;
FORMAT de2 de2f. ;
FORMAT de3 de3f. ;
FORMAT de4 de4f. ;
FORMAT de5 de5f. ;
FORMAT de6 de6f. ;
FORMAT de7 de7f. ;
FORMAT de8 de8f. ;
FORMAT de9 de9f. ;
FORMAT de10 de10f. ;
FORMAT ps1_1 ps1_1f. ;
FORMAT ps1_2 ps1_2f. ;
FORMAT ps1_3 ps1_3f. ;
FORMAT ps1_4 ps1_4f. ;
FORMAT ps2 ps2f. ;
FORMAT ps3 ps3f. ;
FORMAT ps8 ps8f. ;
FORMAT ps11 ps11f. ;
FORMAT ps12 ps12f. ;
FORMAT ps13_ab_1 ps13_ab_1f. ;
FORMAT ps13_ab_2 ps13_ab_2f. ;
FORMAT ps13_ab_3 ps13_ab_3f. ;
FORMAT ps13_ab_4 ps13_ab_4f. ;
FORMAT ps13_ab_5 ps13_ab_5f. ;
FORMAT ps13_ab_6 ps13_ab_6f. ;
FORMAT ps13_ab_7 ps13_ab_7f. ;
FORMAT ps13_c_1 ps13_c_1f. ;
FORMAT ps13_c_2 ps13_c_2f. ;
FORMAT ps13_c_3 ps13_c_3f. ;
FORMAT ps13_c_4 ps13_c_4f. ;
FORMAT ps13_c_5 ps13_c_5f. ;
FORMAT ps13_c_6 ps13_c_6f. ;
FORMAT ps13_c_7 ps13_c_7f. ;
FORMAT ps13_d_1 ps13_d_1f. ;
FORMAT ps13_d_2 ps13_d_2f. ;
FORMAT ps13_d_3 ps13_d_3f. ;
FORMAT ps13_d_4 ps13_d_4f. ;
FORMAT ps13_d_5 ps13_d_5f. ;
FORMAT ps13_d_6 ps13_d_6f. ;
FORMAT ps13_d_7 ps13_d_7f. ;
FORMAT ps15_1 ps15_1f. ;
FORMAT ps15_2 ps15_2f. ;
FORMAT ps15_3 ps15_3f. ;
FORMAT ps18 ps18f. ;
FORMAT ps16 ps16f. ;
FORMAT sa1 sa1f. ;
FORMAT sa3 sa3f. ;
FORMAT sa4 sa4f. ;
FORMAT sa5 sa5f. ;
FORMAT sa6_ab_1 sa6_ab_1f. ;
FORMAT sa6_ab_2 sa6_ab_2f. ;
FORMAT sa6_ab_3 sa6_ab_3f. ;
FORMAT sa6_ab_4 sa6_ab_4f. ;
FORMAT sa6_ab_5 sa6_ab_5f. ;
FORMAT sa6_ab_6 sa6_ab_6f. ;
FORMAT sa6_cd_1 sa6_cd_1f. ;
FORMAT sa6_cd_2 sa6_cd_2f. ;
FORMAT sa6_cd_3 sa6_cd_3f. ;
FORMAT sa6_cd_4 sa6_cd_4f. ;
FORMAT sa6_cd_5 sa6_cd_5f. ;
FORMAT sa6_cd_6 sa6_cd_6f. ;
FORMAT sa24 sa24f. ;
FORMAT sa7_1 sa7_1f. ;
FORMAT sa7_2 sa7_2f. ;
FORMAT sa7_3 sa7_3f. ;
FORMAT sa7_4 sa7_4f. ;
FORMAT sa7_5 sa7_5f. ;
FORMAT sa7_6 sa7_6f. ;
FORMAT sa7_7 sa7_7f. ;
FORMAT sa7_8 sa7_8f. ;
FORMAT sa8_1 sa8_1f. ;
FORMAT sa8_2 sa8_2f. ;
FORMAT sa8_3 sa8_3f. ;
FORMAT sa8_5 sa8_5f. ;
FORMAT sa8_6 sa8_6f. ;
FORMAT sa8_7 sa8_7f. ;
FORMAT sa8_9 sa8_9f. ;
FORMAT sa9_1 sa9_1f. ;
FORMAT sa9_2 sa9_2f. ;
FORMAT sa9_3 sa9_3f. ;
FORMAT sa9_4 sa9_4f. ;
FORMAT sa9_5 sa9_5f. ;
FORMAT sa9_6 sa9_6f. ;
FORMAT sa11 sa11f. ;
FORMAT sa25 sa25f. ;
FORMAT sa13_1 sa13_1f. ;
FORMAT sa13_2 sa13_2f. ;
FORMAT sa13_3 sa13_3f. ;
FORMAT sa13_4 sa13_4f. ;
FORMAT sa13_5 sa13_5f. ;
FORMAT sa14 sa14f. ;
FORMAT sa15_1 sa15_1f. ;
FORMAT sa15_2 sa15_2f. ;
FORMAT sa16_1 sa16_1f. ;
FORMAT sa16_2 sa16_2f. ;
FORMAT sa16_3 sa16_3f. ;
FORMAT sa17 sa17f. ;
FORMAT sa18 sa18f. ;
FORMAT sa19_1 sa19_1f. ;
FORMAT sa19_2 sa19_2f. ;
FORMAT sa19_3 sa19_3f. ;
FORMAT sa19_4 sa19_4f. ;
FORMAT sa19_5 sa19_5f. ;
FORMAT sa19_6 sa19_6f. ;
FORMAT sa19_7 sa19_7f. ;
FORMAT sa19_8 sa19_8f. ;
FORMAT sa23 sa23f. ;
FORMAT sa22_1_1 sa22_1_1f. ;
FORMAT sa22_1_2 sa22_1_2f. ;
FORMAT sa22_1_3 sa22_1_3f. ;
FORMAT sa22_1_4 sa22_1_4f. ;
FORMAT sa22_2_1 sa22_2_1f. ;
FORMAT sa22_2_2 sa22_2_2f. ;
FORMAT sa22_2_3 sa22_2_3f. ;
FORMAT sa22_2_4 sa22_2_4f. ;
FORMAT sa22_3_1 sa22_3_1f. ;
FORMAT sa22_3_2 sa22_3_2f. ;
FORMAT sa22_3_3 sa22_3_3f. ;
FORMAT sa22_3_4 sa22_3_4f. ;
FORMAT sa22_4_1 sa22_4_1f. ;
FORMAT sa22_4_2 sa22_4_2f. ;
FORMAT sa22_4_3 sa22_4_3f. ;
FORMAT sa22_4_4 sa22_4_4f. ;
FORMAT sa22_5_1 sa22_5_1f. ;
FORMAT sa22_5_2 sa22_5_2f. ;
FORMAT sa22_5_3 sa22_5_3f. ;
FORMAT sa22_5_4 sa22_5_4f. ;
FORMAT sa22_6_1 sa22_6_1f. ;
FORMAT sa22_6_2 sa22_6_2f. ;
FORMAT sa22_6_3 sa22_6_3f. ;
FORMAT sa22_6_4 sa22_6_4f. ;
FORMAT sa22_7_1 sa22_7_1f. ;
FORMAT sa22_7_2 sa22_7_2f. ;
FORMAT sa22_7_3 sa22_7_3f. ;
FORMAT sa22_7_4 sa22_7_4f. ;
FORMAT sa22_8_1 sa22_8_1f. ;
FORMAT sa22_8_2 sa22_8_2f. ;
FORMAT sa22_8_3 sa22_8_3f. ;
FORMAT sa22_8_4 sa22_8_4f. ;
FORMAT cs1 cs1f. ;
FORMAT cs5 cs5f. ;
FORMAT cs6 cs6f. ;
FORMAT cs17 cs17f. ;
FORMAT cs18 cs18f. ;
FORMAT cs19_1 cs19_1f. ;
FORMAT cs19_2 cs19_2f. ;
FORMAT cs20 cs20f. ;
FORMAT cs13 cs13f. ;
FORMAT cs14 cs14f. ;
FORMAT cs9_ab cs9_abf. ;
FORMAT cs9_cd_1 cs9_cd_1f. ;
FORMAT cs9_cd_2 cs9_cd_2f. ;
FORMAT cs9_cd_3 cs9_cd_3f. ;
FORMAT cs16_ab_1 cs16_ab_1f. ;
FORMAT cs16_ab_3 cs16_ab_3f. ;
FORMAT cs16_ab_4 cs16_ab_4f. ;
FORMAT cs16_ab_5 cs16_ab_5f. ;
FORMAT cs16_ab_6 cs16_ab_6f. ;
FORMAT cs16_cd_1 cs16_cd_1f. ;
FORMAT cs16_cd_3 cs16_cd_3f. ;
FORMAT cs16_cd_4 cs16_cd_4f. ;
FORMAT cs16_cd_5 cs16_cd_5f. ;
FORMAT cs16_cd_6 cs16_cd_6f. ;
FORMAT cs21_1 cs21_1f. ;
FORMAT cs21_2 cs21_2f. ;
FORMAT cs21_3 cs21_3f. ;
FORMAT cs21_4 cs21_4f. ;
FORMAT cs21_5 cs21_5f. ;
FORMAT cs21_6 cs21_6f. ;
FORMAT cs21_7 cs21_7f. ;
FORMAT cs22 cs22f. ;
FORMAT cs23 cs23f. ;
FORMAT sdassynd_1 sdassynd_1f. ;
FORMAT sdassynd_2 sdassynd_2f. ;
FORMAT sdrel sdrelf. ;
FORMAT sdproxim_1_1 sdproxim_1_1f. ;
FORMAT sdproxim_1_2 sdproxim_1_2f. ;
FORMAT sdproxim_1_3 sdproxim_1_3f. ;
FORMAT sdproxim_1_4 sdproxim_1_4f. ;
FORMAT sdproxim_1_5 sdproxim_1_5f. ;
FORMAT sdproxim_2_1 sdproxim_2_1f. ;
FORMAT sdproxim_2_2 sdproxim_2_2f. ;
FORMAT sdproxim_2_3 sdproxim_2_3f. ;
FORMAT sdproxim_2_4 sdproxim_2_4f. ;
FORMAT sdproxim_2_5 sdproxim_2_5f. ;
FORMAT sdproxim_3_1 sdproxim_3_1f. ;
FORMAT sdproxim_3_2 sdproxim_3_2f. ;
FORMAT sdproxim_3_3 sdproxim_3_3f. ;
FORMAT sdproxim_3_4 sdproxim_3_4f. ;
FORMAT sdproxim_3_5 sdproxim_3_5f. ;
FORMAT sdproxim_4_1 sdproxim_4_1f. ;
FORMAT sdproxim_4_2 sdproxim_4_2f. ;
FORMAT sdproxim_4_3 sdproxim_4_3f. ;
FORMAT sdproxim_4_4 sdproxim_4_4f. ;
FORMAT sdproxim_4_5 sdproxim_4_5f. ;
FORMAT sdproxim_5_1 sdproxim_5_1f. ;
FORMAT sdproxim_5_2 sdproxim_5_2f. ;
FORMAT sdproxim_5_3 sdproxim_5_3f. ;
FORMAT sdproxim_5_4 sdproxim_5_4f. ;
FORMAT sdproxim_5_5 sdproxim_5_5f. ;
FORMAT sdproxim_6_1 sdproxim_6_1f. ;
FORMAT sdproxim_6_2 sdproxim_6_2f. ;
FORMAT sdproxim_6_3 sdproxim_6_3f. ;
FORMAT sdproxim_6_4 sdproxim_6_4f. ;
FORMAT sdproxim_6_5 sdproxim_6_5f. ;
FORMAT sdproxim_7_1 sdproxim_7_1f. ;
FORMAT sdproxim_7_2 sdproxim_7_2f. ;
FORMAT sdproxim_7_3 sdproxim_7_3f. ;
FORMAT sdproxim_7_4 sdproxim_7_4f. ;
FORMAT sdproxim_7_5 sdproxim_7_5f. ;
FORMAT sdproxim_8_1 sdproxim_8_1f. ;
FORMAT sdproxim_8_2 sdproxim_8_2f. ;
FORMAT sdproxim_8_3 sdproxim_8_3f. ;
FORMAT sdproxim_8_4 sdproxim_8_4f. ;
FORMAT sdproxim_8_5 sdproxim_8_5f. ;
FORMAT sdproxim_9_1 sdproxim_9_1f. ;
FORMAT sdproxim_9_2 sdproxim_9_2f. ;
FORMAT sdproxim_9_3 sdproxim_9_3f. ;
FORMAT sdproxim_9_4 sdproxim_9_4f. ;
FORMAT sdproxim_9_5 sdproxim_9_5f. ;
FORMAT sdrichom sdrichomf. ;
FORMAT sdres_1 sdres_1f. ;
FORMAT sdres_2 sdres_2f. ;
FORMAT sdres_3 sdres_3f. ;
FORMAT sdres_4 sdres_4f. ;
FORMAT sdres_5 sdres_5f. ;
FORMAT sdres_6 sdres_6f. ;
FORMAT sdres_7 sdres_7f. ;
FORMAT sdres_8 sdres_8f. ;
FORMAT sdres_9 sdres_9f. ;
FORMAT sdres_10 sdres_10f. ;
FORMAT sdres_11 sdres_11f. ;
FORMAT sdres_12 sdres_12f. ;
FORMAT sdrevtr sdrevtrf. ;
FORMAT sddipl sddiplf. ;
FORMAT sdpol sdpolf. ;
FORMAT sdpol_nw sdpol_nwf. ;
FORMAT sdnat sdnatf. ;
FORMAT sdpnais_det sdpnais_detf. ;
FORMAT sdnatp sdnatpf. ;
FORMAT sdpnaisp_det sdpnaisp_detf. ;
FORMAT sdnatm sdnatmf. ;
FORMAT sdpnaism_det sdpnaism_detf. ;
FORMAT natpar natparf. ;
FORMAT sdnivie sdnivief. ;
RUN;
