/* programme qui cr�e une base avec formats appliqu�s dans la library work */

/* library dans laquelle est stock�e la base du barom�tre */
libname lib "REMPLIR" ;

PROC FORMAT;
value sdsexef 
     1 = "Homme" 
     2 = "Femme" 
;

value habitatf 
     1 = "Rural" 
     2 = "Moins de 20 000 habitants" 
     3 = "De 20 000 � 99 999 habitants" 
     4 = "100 000 habitants et plus" 
     5 = "Agglom�ration parisienne" 
;

value regionf 
     1 = "R�gion parisienne" 
     2 = "Bassin Parisien Est" 
     3 = "Bassin Parisien Ouest" 
     4 = "Nord" 
     5 = "Est" 
     6 = "Ouest" 
     7 = "Sud Ouest" 
     8 = "Sud Est" 
     9 = "M�diterran�e" 
;

value sdagetrf 
     1 = "18 � 24 ans" 
     2 = "25 � 34 ans" 
     3 = "35 � 49 ans" 
     4 = "50 � 64 ans" 
     5 = "65 ans et +" 
;

value sdsituaf 
     1 = "Vous travaillez � temps plein" 
     2 = "Vous travaillez � temps partiel" 
     3 = "Vous travaillez de fa�on intermittente" 
     4 = "Vous �tes � la recherche d'un emploi" 
     5 = "Vous �tes �tudiant.e" 
     6 = "Vous �tes retrait�.e ou pr�retrait�.e" 
     7 = "Vous n'exercez aucune activit� professionnelle" 
     8 = "Vous n'exercez aucune activit� professionnelle ou �tes retrait�.e (avant 2014)" 
;

value sdactf 
     1 = "Salari�.e du secteur priv�" 
     2 = "Salari�.e du secteur public" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Ind�pendant.e employeur.euse" 
     5 = "� la recherche d'un premier emploi" 
     6 = "�l�ve, �tudiant.e, en formation, ou en stage non r�mun�r�" 
     7 = "Apprenti.e sous contrat ou stagiaire r�mun�r�.e" 
     8 = "Au foyer" 
     9 = "Autre inactif.ive" 
;

value sdstatf 
     1 = "      Salari�.e du secteur public" 
     2 = "      Salari�.e du secteur priv�" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Employeur.euse" 
     5 = "Ch�meur.euse" 
     6 = "Inactif.ive" 
     7 = "Autre" 
;

value sdpcs7f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Autre inactif.ive" 
     8 = "(Sans r�ponse)" 
;

value sdpcs10f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Ch�meur.euse" 
     8 = "Retrait�.e" 
     9 = "Au foyer" 
     10 = "Autre inactif.ive" 
     11 = "(Sans r�ponse)" 
;

value sdstatempf 
     1 = "CDI (ou fonctionnaire titulaire, y compris fonctionnaire stagiaire)" 
     2 = "CDD" 
     3 = "Int�rim" 
     4 = "Sans contrat" 
     5 = "(NSP)" 
;

value sdsitfamf 
     1 = "Vous vivez seul.e" 
     2 = "Vous �tes un membre du couple" 
     3 = "Vous �tes l'unique parent du foyer" 
     4 = "Vous �tes un enfant de la famille" 
     5 = "Vous �tes un.e ami.e ou un.e parent.e h�berg�.e par la famille" 
     6 = "Vous �tes un.e colocataire" 
     7 = "Autres (personnel de maison, ...)" 
;

value sdprf 
     1 = "Votre conjoint.e" 
     2 = "Votre p�re (ou de votre m�re si votre p�re ne vit pas au foyer)" 
     3 = "La personne de r�f�rence du m�nage" 
     4 = "(Vous-m�me)" 
;

value sdprsituaf 
     1 = "Il.elle travaille � temps plein" 
     2 = "Il.elle travaille � temps partiel" 
     3 = "Il.elle travaille de fa�on intermittente" 
     4 = "Il.elle est � la recherche d'un emploi, au ch�mage" 
     5 = "Il.elle est �tudiant.e" 
     6 = "Il.elle est retrait�.e ou pr�retrait�.e" 
     7 = "Il.elle n'exerce aucune activit� professionnelle" 
;

value sdpractf 
     1 = "Salari�.e du secteur priv�" 
     2 = "Salari�.e du secteur public" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Ind�pendant.e employeur.euse" 
     5 = "� la recherche d'un premier emploi" 
     6 = "�l�ve, �tudiant.e, en formation, ou en stage non r�mun�r�" 
     7 = "Apprenti.e sous contrat ou stagiaire r�mun�r�.e" 
     8 = "Au foyer" 
     9 = "Autre inactif.ive" 
;

value sdprstatf 
     1 = "Salari�.e du secteur public" 
     2 = "Salari�.e du secteur priv�" 
     3 = "Ind�pendant.e sans salari�.e" 
     4 = "Employeur.euse" 
     5 = "Ch�meur.euse" 
     6 = "Inactif.ive" 
     7 = "Autre" 
;

value sdprpcs7f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Autre inactif.ive" 
     8 = "(Sans r�ponse)" 
;

value sdprpcs10f 
     1 = "Agriculteur.trice" 
     2 = "Artisan.e ou commer�ant.e" 
     3 = "Profession lib�rale, cadre sup�rieur.e" 
     4 = "Profession interm�diaire" 
     5 = "Employ�.e" 
     6 = "Ouvrier.�re" 
     7 = "Ch�meur.euse" 
     8 = "Retrait�.e" 
     9 = "Au foyer" 
     10 = "Autre inactif.ive" 
;

value sdnbpersf 
     1 = "1 personne" 
     2 = "2 personnes" 
     3 = "3 personnes" 
     4 = "4 personnes" 
     5 = "5 personnes" 
     6 = "6 personnes" 
     7 = "7 personnes" 
     8 = "8 personnes" 
     9 = "9 personnes et plus" 
     10 = "10 personnes et plus (depuis 2013)" 
;

value sdcouplef 
     1 = "Oui" 
     2 = "Non" 
;

value sdmatrif 
     1 = "Mari�.e, pacs�.e ou en concubinage" 
     2 = "Veuf ou veuve" 
     3 = "C�libataire" 
;

value sdnbenff 
     1 = "Aucun enfant" 
     2 = "1 enfant" 
     3 = "2 enfants" 
     4 = "3 enfants" 
     5 = "4 enfants" 
     6 = "5 enfants" 
     7 = "6 enfants" 
     8 = "7 enfants" 
     9 = "8 enfants" 
     10 = "9 enfants" 
     11 = "10 enfants et plus" 
     12 = "(NSP)" 
;

value sdnbaduf 
     1 = "1 adulte" 
     2 = "2 adultes" 
     3 = "3 adultes" 
     4 = "4 adultes" 
     5 = "5 adultes" 
     6 = "6 adultes" 
     7 = "7 adultes" 
     8 = "8 adultes" 
     9 = "9 adultes" 
     10 = "10 adultes et +" 
;

value sdsplitf 
     1 = "Questionnaire A" 
     2 = "Questionnaire B" 
     3 = "Questionnaire C" 
     4 = "Questionnaire D" 
;

value sdmutf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og1f 
     1 = "Tr�s bonne" 
     2 = "Assez bonne" 
     3 = "Assez mauvaise" 
     4 = "Tr�s mauvaise" 
     5 = "(NSP)" 
;

value og2f 
     1 = "Bien meilleure" 
     2 = "Plut�t meilleure" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bonne" 
     5 = "Bien moins bonne" 
     6 = "(NSP)" 
;

value og3_1f 
     1 = "Tr�s optimiste" 
     2 = "Plut�t optimiste" 
     3 = "Plut�t pessimiste" 
     4 = "Tr�s pessimiste" 
     5 = "(NSP)" 
;

value og3_2f 
     1 = "Tr�s optimiste" 
     2 = "Plut�t optimiste" 
     3 = "Plut�t pessimiste" 
     4 = "Tr�s pessimiste" 
     5 = "(NSP)" 
;

value me1f 
     1 = "Oui, certainement" 
     2 = "Oui, probablement" 
     3 = "Non probablement pas" 
     4 = "Non, certainement pas" 
     5 = "(NSP)" 
;

value og4_1f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_2f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_3f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_4f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_5f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_6f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_7f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_8f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og4_9f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_1f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_2f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_3f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_4f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_5f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_6f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_ab_7f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_cd_1f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_cd_4f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og5_cd_7f 
     1 = "Beaucoup" 
     2 = "Assez" 
     3 = "Peu" 
     4 = "Pas du tout" 
     5 = "(NSP)" 
;

value og6f 
     1 = "Il faut radicalement changer la soci�t� fran�aise" 
     2 = "Il faut r�former la soci�t� fran�aise sur certains points tout en en conservant l'essentiel" 
     3 = "Il faut conserver la soci�t� fran�aise en l'�tat" 
     4 = "(NSP)" 
;

value og7f 
     1 = "Trop" 
     2 = "Ce qu'il faut" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value og8_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_3f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_4f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_5f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_6f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_8f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_9f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og8_11f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value og9f 
     1 = "Le m�rite individuel" 
     2 = "Le hasard des circonstances" 
     3 = "Le milieu social de naissance" 
     4 = "(Les trois � parts �gales)" 
     5 = "(NSP)" 
;

value og10f 
     1 = "Majoritairement par vous-m�me" 
     2 = "Majoritairement par votre conjoint.e" 
     3 = "A part �gale par vous et votre conjoint.e" 
     4 = "(NSP)" 
;

value og11f 
     1 = "Les hommes en font moins que les femmes" 
     2 = "Les hommes en font autant que les femmes" 
     3 = "Les hommes en font plus que les femmes" 
     4 = "(NSP)" 
;

value og12f 
     1 = "Les hommes en font moins qu'avant" 
     2 = "Les hommes en font autant qu'avant" 
     3 = "Les hommes en font plus qu'avant" 
     4 = "(NSP)" 
;

value og13_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_8f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_9f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_10f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13_11f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value og13bis_1f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_2f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_3f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_4f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_5f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_6f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_7f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_8f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_9f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_10f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13bis_11f 
     1 = "Oui, assez pr�cis�ment" 
     2 = "Oui, mais approximativement" 
     3 = "Non" 
     4 = "(NSP)" 
;

value og13ter_1f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_2f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_3f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_4f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_5f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_6f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_7f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_8f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_9f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_10f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value og13ter_11f 
     1 = "Non, je n'en ai pas entendu parler" 
     2 = "Oui, j'en ai entendu parler mais je ne sais pas qui peut en b�n�ficier" 
     3 = "Oui, j'en ai entendu parler et je sais qui peut en b�n�ficier" 
     4 = "(NSP)" 
;

value in1f 
     1 = "Plut�t juste" 
     2 = "Plut�t injuste" 
     3 = "(NSP)" 
;

value in2f 
     1 = "Ont plut�t augment�" 
     2 = "Ont plut�t diminu�" 
     3 = "(Sont rest�es stables)" 
     4 = "(NSP)" 
;

value in3f 
     1 = "Vont plut�t augmenter" 
     2 = "Vont plut�t diminuer" 
     3 = "(Resteront stables)" 
     4 = "(NSP)" 
;

value in4_abf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "Les in�galit�s entre les femmes et les hommes" 
     10 = "(NSP)" 
;

value in4_cdf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "Les in�galit�s entre les femmes et les hommes" 
     10 = "(NSP)" 
;

value in5_abf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "Les in�galit�s entre les femmes et les hommes" 
     10 = "(NSP)" 
;

value in5_cdf 
     1 = "Les in�galit�s de revenus" 
     2 = "Les in�galit�s de logement" 
     3 = "Les in�galit�s li�es � l'h�ritage familial" 
     4 = "Les in�galit�s par rapport au type d'emploi" 
     5 = "Les in�galit�s dans les �tudes scolaires" 
     6 = "Les in�galit�s d'acc�s aux soins" 
     7 = "Les in�galit�s par rapport au fait d'avoir un emploi" 
     8 = "Les in�galit�s li�es � l'origine ethnique" 
     9 = "Les in�galit�s entre les femmes et les hommes" 
     10 = "(NSP)" 
;

value in6f 
     1 = "Tr�s importantes" 
     2 = "Assez importantes" 
     3 = "Assez faibles" 
     4 = "Tr�s faibles" 
     5 = "(NSP)" 
;

value in7f 
     1 = "Plut�t augment�" 
     2 = "Plut�t diminu�" 
     3 = "N'ont pas �volu�" 
     4 = "(NSP)" 
;

value in8f 
     1 = "10 Foyers aux revenus les plus �lev�s" 
     2 = "9" 
     3 = "8" 
     4 = "7" 
     5 = "6" 
     6 = "5" 
     7 = "4" 
     8 = "3" 
     9 = "2" 
     10 = "1 Foyers aux revenus les plus bas" 
     11 = "(NSP)" 
;

value in9f 
     1 = "10 Foyers aux revenus les plus �lev�s" 
     2 = "9" 
     3 = "8" 
     4 = "7" 
     5 = "6" 
     6 = "5" 
     7 = "4" 
     8 = "3" 
     9 = "2" 
     10 = "1 Foyers aux revenus les plus bas" 
     11 = "(NSP)" 
;

value me2f 
     1 = "Souvent" 
     2 = "De temps en temps" 
     3 = "Rarement" 
     4 = "Jamais" 
     5 = "(NSP)" 
;

value in14_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value in14_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value in14_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe1f 
     1 = "Ont diminu�" 
     2 = "Ont augment�" 
     3 = "(Sont rest�es stables)" 
     4 = "(NSP)" 
;

value pe2f 
     1 = "Vont plut�t diminuer" 
     2 = "Vont plut�t augmenter" 
     3 = "(Resteront stables)" 
     4 = "(NSP)" 
;

value pe3f 
     1 = "Oui, plut�t" 
     2 = "Non, plut�t pas" 
     3 = "Je me consid�re d�j� comme pauvre" 
     4 = "(NSP)" 
;

value pe5_1f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe5_2f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe5_3f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe5_4f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe5_5f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe5_6f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe4_1f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe4_2f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe4_3f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe4_4f 
     1 = "Plus" 
     2 = "Autant" 
     3 = "Moins" 
     4 = "(NSP)" 
;

value pe6_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value pe6_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value pe6_3f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value pe6_4f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value pe9f 
     1 = "Il faut augmenter le RSA" 
     2 = "Il faut diminuer le RSA" 
     3 = "(Il faut laisser le RSA � ce niveau)" 
     4 = "(NSP)" 
;

value pe10f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe17f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe12f 
     1 = "Il faut augmenter le SMIC" 
     2 = "Il faut diminuer le SMIC" 
     3 = "Il faut laisser le SMIC � ce niveau" 
     4 = "(NSP)" 
;

value pe13_abf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe13_cdf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe14_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe14_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe14_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe14_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value pe15f 
     1 = "Vous �tes suffisamment aid�.e par les pouvoirs publics, ou n'avez pas besoin d'�tre aid�.e" 
     2 = "Vous auriez besoin d'�tre aid�.e davantage par les pouvoir publics" 
     3 = "(Non concern�.e)" 
     4 = "(NSP)" 
;

value me3f 
     1 = "Que vous y feriez face sans trop de probl�me" 
     2 = "Qu�il vous serait assez difficile d�y faire face" 
     3 = "Qu�il vous serait tr�s difficile d�y faire face" 
     4 = "(NSP)" 
;

value lo1f 
     1 = "Propri�taire" 
     2 = "Locataire d'un logement social (y compris HLM)" 
     3 = "Locataire hors logement social, c'est-�-dire dans le parc priv�" 
     4 = "Log� gratuitement" 
     5 = "(NSP)" 
     6 = "Locataire (item pour historique)" 
;

value lo2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value re1f 
     1 = "Bien meilleur" 
     2 = "Plut�t meilleur" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bon" 
     5 = "Bien moins bon" 
     6 = "(NSP)" 
;

value re2f 
     1 = "Bien meilleur" 
     2 = "Plut�t meilleur" 
     3 = "A peu pr�s identique" 
     4 = "Plut�t moins bon" 
     5 = "Bien moins bon" 
     6 = "(NSP)" 
;

value re3f 
     1 = "Aucune baisse de revenu" 
     2 = "-10%" 
     3 = "-20%" 
     4 = "-30%" 
     5 = "- 40 % ou plus" 
     6 = "(NSP)" 
;

value re9bisf 
     1 = "Les r�formes d�j� effectu�es sont suffisantes" 
     2 = "Des r�formes suppl�mentaires sont n�cessaires" 
     3 = "(NSP)" 
;

value re9f 
     1 = "Il faudrait allonger la dur�e de cotisation" 
     2 = "Il faudrait reculer l'�ge de la retraite" 
     3 = "Il faudrait augmenter les cotisations pesant sur les salari�s" 
     4 = "Il faudrait diminuer les pensions vers�es aux retrait�s" 
     5 = "(Il faudrait un autre type de r�forme)" 
     6 = "(NSP)" 
;

value re10f 
     1 = "Il faut garder le syst�me actuel fond� sur la r�partition, en le r�formant" 
     2 = "Il faut ajouter au syst�me actuel un compl�ment d'assurance ou d'�pargne individuelle" 
     3 = "(Aucune de ces 2 propositions)" 
     4 = "(NSP)" 
     5 = "(NSP / Aucune de ces 2 propositions (avant 2014))" 
;

value fa1f 
     1 = "Soutenir la natalit�" 
     2 = "Rapprocher les niveaux de vie des familles avec enfants et des personnes sans enfant" 
     3 = "Permettre aux familles de mieux se loger" 
     4 = "Permettre une meilleure conciliation entre vie familiale et vie professionnelle" 
     5 = "Rendre les jeunes de plus de 20 ans plus autonomes � l'�gard de leur famille" 
     6 = "(NSP)" 
;

value fa2f 
     1 = "Les familles nombreuses (c'est-�-dire celles qui sont compos�es de trois enfants et plus)" 
     2 = "Les familles dont les deux parents travaillent et qui ont de jeunes enfants" 
     3 = "Les familles qui ont des enfants de plus de 20 ans encore � charge" 
     4 = "Les familles monoparentales, c'est-�-dire les p�res ou les m�res �levant seuls leurs enfants" 
     5 = "Les familles aux revenus les plus modestes" 
     6 = "Les jeunes parents d�s le premier enfant" 
     7 = "(Il faut aider toutes les familles indiff�remment)" 
     8 = "(NSP)" 
;

value fa3_abf 
     1 = "La cr�che et autres accueils collectifs" 
     2 = "Les grands-parents" 
     3 = "Une personne r�mun�r�e � son domicile (assistant(e) maternel(le))" 
     4 = "Une personne r�mun�r�e au domicile de l'enfant" 
     5 = "(NSP)" 
;

value fa3_cdf 
     1 = "La cr�che et autres accueils collectifs" 
     2 = "Les grands-parents" 
     3 = "Une personne r�mun�r�e � son domicile (assistant(e) maternel(le))" 
     4 = "Une personne r�mun�r�e au domicile de l'enfant" 
     5 = "(NSP)" 
;

value fa4f 
     1 = "Vous-m�me ou votre conjoint(e)" 
     2 = "La cr�che et autres accueils collectifs" 
     3 = "Les grands-parents" 
     4 = "Une personne r�mun�r�e � son domicile (assistant(e) maternel(le))" 
     5 = "Une personne r�mun�r�e au domicile de l'enfant" 
     6 = "Autre" 
     7 = "(NSP / Sans objet)" 
;

value fa5f 
     1 = "Les femmes doivent pouvoir b�n�ficier d'am�nagement du temps de travail plus que les hommes" 
     2 = "Les hommes doivent b�n�ficier d'am�nagement du temps de travail au m�me titre que les femmes" 
     3 = "(Les hommes doivent pouvoir b�n�ficier d'am�nagement du temps de travail plus que les femmes)" 
     4 = "(NSP)" 
;

value fa17f 
     1 = "Qu'il ne doit pas percevoir de prestation" 
     2 = "Qu'il doit percevoir une allocation fixe" 
     3 = "Qu'il doit percevoir une allocation qui d�pende de son salaire ant�rieur" 
     4 = "(NSP)" 
;

value fa6f 
     1 = "Trop courtes" 
     2 = "Suffisantes" 
     3 = "Trop longues" 
     4 = "(NSP)" 
;

value fa7f 
     1 = "Allonger sa dur�e" 
     2 = "Maintenir sa dur�e actuelle" 
     3 = "R�duire sa dur�e" 
     4 = "(NSP)" 
;

value fa8f 
     1 = "Oui, car ce cong� est n�cessaire pour tous les p�res" 
     2 = "Non car il faut laisser la libert� aux p�res de choisir s'ils veulent le prendre" 
     3 = "(NSP)" 
;

value fa9_1f 
     1 = "Non" 
     2 = "Oui, principalement sous forme de groupes de parole permettant aux parents d'�changer entre eux" 
     3 = "Oui, principalement sous forme d'entretiens individuels avec des professionnels" 
     4 = "Oui, principalement sous forme d'une mise � disposition d'informations (site internet, brochure...)" 
     5 = "(NSP)" 
;

value fa9_2f 
     1 = "Non" 
     2 = "Oui, principalement sous forme de groupes de parole permettant aux parents d'�changer entre eux" 
     3 = "Oui, principalement sous forme d'entretiens individuels avec des professionnels" 
     4 = "Oui, principalement sous forme d'une mise � disposition d'informations (site internet, brochure...)" 
     5 = "(NSP)" 
;

value fa9_3f 
     1 = "Non" 
     2 = "Oui, principalement sous forme de groupes de parole permettant aux parents d'�changer entre eux" 
     3 = "Oui, principalement sous forme d'entretiens individuels avec des professionnels" 
     4 = "Oui, principalement sous forme d'une mise � disposition d'informations (site internet, brochure...)" 
     5 = "(NSP)" 
;

value fa9_4f 
     1 = "Non" 
     2 = "Oui, principalement sous forme de groupes de parole permettant aux parents d'�changer entre eux" 
     3 = "Oui, principalement sous forme d'entretiens individuels avec des professionnels" 
     4 = "Oui, principalement sous forme d'une mise � disposition d'informations (site internet, brochure...)" 
     5 = "(NSP)" 
;

value fa9_5f 
     1 = "Non" 
     2 = "Oui, principalement sous forme de groupes de parole permettant aux parents d'�changer entre eux" 
     3 = "Oui, principalement sous forme d'entretiens individuels avec des professionnels" 
     4 = "Oui, principalement sous forme d'une mise � disposition d'informations (site internet, brochure...)" 
     5 = "(NSP)" 
;

value fa10f 
     1 = "Qu'ils soient gard�s par la m�re" 
     2 = "Qu'ils soient gard�s par le p�re" 
     3 = "Qu'ils soient gard�s par l'un des deux parents, peu importe lequel" 
     4 = "Que la garde soit partag�e entre les deux parents" 
     5 = "(NSP)" 
;

value fa18f 
     1 = "Que l'�tat doit intervenir pour assurer que la pension alimentaire est correctement pay�e" 
     2 = "Que c'est une affaire priv�e entre ex-conjoints, dans laquelle l'�tat ne doit pas intervenir" 
     3 = "(NSP)" 
;

value fa11f 
     1 = "Il faudrait accorder des allocations familiales d�s le premier enfant, quitte � les diminuer � partir de 2 enfants" 
     2 = "Il faut maintenir le syst�me actuel qui accorde des allocations familiales � partir du deuxi�me enfant" 
     3 = "Il ne faut plus donner d'allocations familiales" 
     4 = "(NSP)" 
;

value fa12f 
     1 = "Il faudrait que l'on touche la m�me somme d'argent par enfant, qu'il s'agisse du deuxi�me ou du troisi�me enfant" 
     2 = "Il faut maintenir le syst�me actuel qui apporte une aide plus importante aux familles nombreuses" 
     3 = "Il ne faut plus donner d'allocations familiales" 
     4 = "(NSP)" 
;

value fa13f 
     1 = "R�server les allocations familiales aux familles dont le revenu total est inf�rieur � 6000 �" 
     2 = "Donner des allocations familiales � toutes les familles mais davantage si leur revenu est inf�rieur � 6000 �" 
     3 = "Donner autant d'allocations familiales � toutes les familles" 
     4 = "Il ne faut plus donner d'allocations familiales" 
     5 = "(NSP)" 
;

value fa14f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value fa15f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value fa16f 
     1 = "Oui, les couples h�t�rosexuels et les couples homosexuels" 
     2 = "Oui, mais seulement les couples h�t�rosexuels" 
     3 = "Oui, mais seulement les couples homosexuels" 
     4 = "Non" 
     5 = "(NSP)" 
;

value ha1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(NSP)" 
;

value de1f 
     1 = "L'�tat et les pouvoirs publics" 
     2 = "Les enfants ou les familles des personnes �g�es d�pendantes" 
     3 = "Les personnes �g�es d�pendantes elles-m�mes, en �pargnant ou souscrivant une assurance priv�e" 
     4 = "(NSP)" 
;

value de4f 
     1 = "Tout � fait pr�t.e" 
     2 = "Plut�t pr�t.e" 
     3 = "Plut�t pas pr�t.e" 
     4 = "Pas du tout pr�t.e" 
     5 = "(NSP)" 
;

value de5f 
     1 = "Vous le placeriez dans une institution sp�cialis�e" 
     2 = "Vous l'accueilleriez chez vous" 
     3 = "Vous consacreriez une partie de votre revenu � lui payer des aides de mani�re � ce qu'il reste � son domicile" 
     4 = "Vous feriez en sorte de pouvoir vous en occuper � son domicile" 
     5 = "(NSP)" 
;

value de7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "Non car j'ai moi-m�me besoin d'aide pour ces activit�s" 
     4 = "(NSP)" 
;

value ps1_1f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "� tous sans distinction de cat�gories sociales et de statut professionnel" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps1_2f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "� tous sans distinction de cat�gories sociales et de statut professionnel" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps1_3f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "� tous sans distinction de cat�gories sociales et de statut professionnel" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps1_4f 
     1 = "Uniquement � ceux qui cotisent" 
     2 = "Uniquement � ceux qui ne peuvent pas ou n'ont pas les moyens de s'en sortir seuls" 
     3 = "� tous sans distinction de cat�gories sociales et de statut professionnel" 
     4 = "Davantage � ceux qui cotisent, avec un niveau minimal de protection pour les autres" 
     5 = "(NSP)" 
;

value ps2f 
     1 = "Il est souhaitable que les entreprises cotisent davantage pour la protection sociale" 
     2 = "Il est souhaitable que les entreprises cotisent moins pour la protection sociale" 
     3 = "Les entreprises ne doivent ni plus, ni moins cotiser qu'actuellement" 
     4 = "(NSP)" 
;

value ps3f 
     1 = "Excessif" 
     2 = "Normal" 
     3 = "Insuffisant" 
     4 = "(NSP)" 
;

value ps13_1f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_2f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_3f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_4f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_5f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_6f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps13_7f 
     1 = "Oui, tout � fait" 
     2 = "Oui, plut�t" 
     3 = "Non, plut�t pas" 
     4 = "Non, pas du tout" 
     5 = "(Non concern�.e)" 
     6 = "(NSP)" 
;

value ps15_1f 
     1 = "Totalement d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas d'accord du tout" 
     5 = "(NSP)" 
;

value ps15_2f 
     1 = "Totalement d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas d'accord du tout" 
     5 = "(NSP)" 
;

value ps15_3f 
     1 = "Totalement d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas d'accord du tout" 
     5 = "(NSP)" 
;

value ps18f 
     1 = "Une augmentation des prestations (allocations logement, allocations familiales, minima sociaux,...)" 
     2 = "Le d�veloppement de services (cr�ches, dispositifs de formation, �quipements pour personnes �g�es,...)" 
     3 = "Un meilleur accompagnement vers les droits (agents accompagnateurs, aide � l�utilisation du num�rique, �)" 
     4 = "(NSP)" 
;

value sa1f 
     1 = "Tr�s bon" 
     2 = "Bon" 
     3 = "Moyen" 
     4 = "Mauvais" 
     5 = "Tr�s mauvais" 
     6 = "(NSP)" 
;

value sa3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sa4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sa5f 
     1 = "Beaucoup am�lior�" 
     2 = "Un peu am�lior�" 
     3 = "Un peu d�t�rior�" 
     4 = "Beaucoup d�t�rior�" 
     5 = "Est rest� identique" 
     6 = "(NSP)" 
;

value sa6_ab_1f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_2f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_3f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_4f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_5f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_ab_6f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_1f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_2f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_3f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_4f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_5f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value sa6_cd_6f 
     1 = "Plut�t d'accord" 
     2 = "Plut�t pas d'accord" 
     3 = "(NSP)" 
;

value me4f 
     1 = "Oui, tr�s inquiet.e" 
     2 = "Oui, assez inquiet.e" 
     3 = "Non, pas tr�s inquiet.e" 
     4 = "Non, pas inquiet.e du tout" 
     5 = "(NSP)" 
;

value sa7_1f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_2f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_3f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_4f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_5f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_6f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_7f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa7_8f 
     1 = "Tr�s satisfait.e" 
     2 = "Plut�t satisfait.e" 
     3 = "Ni satisfait.e ni insatisfait.e" 
     4 = "Plut�t insatisfait.e" 
     5 = "Tr�s insatisfait.e" 
     6 = "(NSP)" 
     7 = "(Je ne suis jamais all�.e/ je ne connais pas)" 
;

value sa8_1f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_2f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_3f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_5f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_6f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_7f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa8_9f 
     1 = "Plut�t favorable" 
     2 = "Plut�t oppos�.e" 
     3 = "(NSP)" 
;

value sa9_1f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_2f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_3f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_4f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_5f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa9_6f 
     1 = "Trop" 
     2 = "Suffisamment" 
     3 = "Pas assez" 
     4 = "(NSP)" 
;

value sa22_1f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_2f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_3f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_4f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_5f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_6f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_7f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa22_8f 
     1 = "Oui pour moi" 
     2 = "Oui mais uniquement pour mes proches ou ma famille" 
     3 = "Non" 
     4 = "(NSP)" 
;

value sa26_1f 
     1 = "Moins de 6 mois" 
     2 = "Entre 6 mois et 1 an" 
     3 = "Plus de 1 an" 
     4 = "(NSP)" 
;

value sa26_2f 
     1 = "Moins de 6 mois" 
     2 = "Entre 6 mois et 1 an" 
     3 = "Plus de 1 an" 
     4 = "(NSP)" 
;

value cs1f 
     1 = "Tr�s forte" 
     2 = "Assez forte" 
     3 = "Pas tr�s forte" 
     4 = "Pas du tout forte" 
     5 = "(NSP)" 
;

value cs24f 
     1 = "Elle a augment�" 
     2 = "Elle a diminu�" 
     3 = "Elle n�a ni augment� ni diminu�" 
     4 = "(NSP)" 
;

value cs4f 
     1 = "� votre commune, � votre quartier" 
     2 = "� votre d�partement" 
     3 = "� votre r�gion" 
     4 = "� la France" 
     5 = "� un autre pays que la France" 
     6 = "� l'Europe" 
     7 = "Au monde" 
     8 = "(NSP)" 
;

value cs5_abf 
     1 = "Tr�s bien int�gr�.e" 
     2 = "Assez bien int�gr�.e" 
     3 = "Pas bien int�gr�.e" 
     4 = "Pas int�gr�.e du tout" 
     5 = "(NSP)" 
;

value cs5_cdf 
     1 = "Tr�s bien int�gr�.e" 
     2 = "Assez bien int�gr�.e" 
     3 = "Pas bien int�gr�.e" 
     4 = "Pas int�gr�.e du tout" 
     5 = "(NSP)" 
;

value cs17f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs18f 
     1 = "Les pouvoirs publics font trop" 
     2 = "Les pouvoirs publics font ce qu'ils doivent" 
     3 = "Les pouvoirs publics ne font pas assez pour les plus d�munis" 
     4 = "(NSP)" 
;

value cs19_1f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value cs19_2f 
     1 = "Tout � fait d'accord" 
     2 = "Plut�t d'accord" 
     3 = "Plut�t pas d'accord" 
     4 = "Pas du tout d'accord" 
     5 = "(NSP)" 
;

value cs20f 
     1 = "Par manque d'information sur les aides, ou sur les organismes � qui s'adresser" 
     2 = "Parce que les d�marches � faire sont trop complexes et trop longues" 
     3 = "Parce qu'elles pr�f�rent s'en sortir par elles-m�mes" 
     4 = "Parce qu'elles refusent de d�pendre de l'aide sociale et d'�tre consid�r�es comme des assist�es" 
     5 = "Pour ne pas avoir � rendre des comptes, � faire l'objet de contr�le" 
     6 = "Pour ne pas subir des cons�quences n�gatives (perte d'autres droits, imp�ts, probl�mes administratifs, etc.)" 
     7 = "Parce que ces aides n'apportent pas grand-chose financi�rement" 
     8 = "Autre" 
     9 = "(NSP)" 
;

value cs13f 
     1 = "Veiller � ce que chacun puisse �tre libre de ses croyances et de ses pratiques religieuses" 
     2 = "Veiller � ce que les croyances et les pratiques religieuses ne soient pas visibles dans les espaces publics" 
     3 = "(NSP)" 
;

value cs14f 
     1 = "La diversit� des cultures et des origines est une richesse pour notre pays" 
     2 = "La diversit� des cultures et des origines rend difficile la vie en commun dans notre pays" 
     3 = "(NSP)" 
;

value cs9f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs21_7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value cs22f 
     1 = "Entre les diff�rentes g�n�rations" 
     2 = "Entre les diff�rentes cat�gories socio-�conomiques" 
     3 = "Entre les diff�rents partis ou tendances politiques" 
     4 = "Entre les diff�rents territoires" 
     5 = "Entre les diff�rentes confessions religieuses" 
     6 = "Entre les personnes d�origines diff�rentes" 
     7 = "(NSP)" 
;

value cs23f 
     1 = "Moins" 
     2 = "� peu pr�s ce chiffre" 
     3 = "Davantage" 
     4 = "Aucun migrant" 
     5 = "(Autre crit�re de r�partition)" 
     6 = "(NSP)" 
;

value sdassynd_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdassynd_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdrelf 
     1 = "Une pratique religieuse r�guli�re" 
     2 = "Une pratique occasionnelle ou le sentiment d'appartenir � une religion" 
     3 = "Ni pratique ni sentiment d'appartenance" 
     4 = "(NSP ou souhaite ne pas r�pondre)" 
;

value sdproxim_1f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_2f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_3f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_4f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_5f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_6f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_7f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_8f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdproxim_9f 
     1 = "Non" 
     2 = "Oui, dans votre famille" 
     3 = "Oui, hors famille" 
     4 = "Oui, vous-m�me" 
     5 = "Oui, dans votre famille et hors famille" 
     6 = "Oui, vous-m�me et dans votre famille" 
     7 = "Oui, vous-m�me et hors famille" 
     8 = "Oui, vous-m�me, dans votre famille et hors famille" 
     9 = "(NSP)" 
;

value sdrichom_abf 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdrichom_cd_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdrichom_cd_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_1f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_2f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_3f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_4f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_5f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_6f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_7f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_8f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_9f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_10f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_11f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdres_12f 
     1 = "Oui" 
     2 = "Non" 
     3 = "(NSP)" 
;

value sdrevtrf 
     1 = "Moins de 1000 euros par mois" 
     2 = "De 1000 � moins de 1400 euros par mois" 
     3 = "De 1400 � moins de 1900 euros par mois" 
     4 = "De 1900 � moins de 2400 euros par mois" 
     5 = "De 2400 � moins de 3800 euros par mois" 
     6 = "De 3800 � moins de 5300 euros par mois" 
     7 = "Plus de 5300 euros par mois" 
     8 = "(NSP/refus de r�ponse)" 
;

value me5f 
     1 = "Plut�t stables" 
     2 = "Plut�t variables" 
     3 = "(NSP)" 
;

value me6f 
     1 = "Plut�t augmenter" 
     2 = "Plut�t diminuer" 
     3 = "Rester stables" 
     4 = "(NSP)" 
;

value sddiplf 
     1 = "Vous n'avez pas de dipl�me" 
     2 = "Vous avez un certificat d'�tudes primaires" 
     3 = "Vous avez un ancien brevet, BEPC, ou brevet des coll�ges" 
     4 = "Vous avez un certificat d'aptitude professionnelle (CAP), un brevet  d'enseignement professionnel (BEP)" 
     5 = "Vous avez un bac d'enseignement g�n�ral" 
     6 = "Vous avez un bac d'enseignement technologique ou professionnel" 
     7 = "Vous avez un bac + 2 ans ou niveau bac + 2 ans (DUT, BTS, DEUG, L2)" 
     8 = "Vous avez un dipl�me sup�rieur (2�me, 3�me cycle, grande �cole, L3, M1, M2)" 
     9 = "(Autre situation)" 
     10 = "(NSP)" 
;

value sdpolf 
     1 = "0 Tr�s � gauche" 
     2 = "1" 
     3 = "2" 
     4 = "3" 
     5 = "4" 
     6 = "5" 
     7 = "6" 
     8 = "7" 
     9 = "8" 
     10 = "9" 
     11 = "10 Tr�s � droite" 
     12 = "(NSP)" 
;

value sdpoltrf 
     1 = "0-2 extr�me gauche" 
     2 = "3-4 gauche mod�r�e" 
     3 = "5 centre" 
     4 = "6-7 droite mod�r�e" 
     5 = "8-10 extr�me droite" 
     6 = "(NSP)" 
;

value sdnatf 
     1 = "N�.e fran�ais.e" 
     2 = "Devenu.e fran�ais.e par acquisition (naturalisation, mariage, d�claration, option � votre majorit�...)" 
     3 = "�tranger.�re" 
     4 = "(NSP)" 
;

value sdpnaistrf 
     1 = "France" 
     2 = "Pays europ�en" 
     3 = "Pays non europ�en" 
     4 = "Autres" 
     5 = "(NSP)" 
;

value sdanfrtrf 
     1 = "Avant 1970" 
     2 = "De 1970 � 1989" 
     3 = "Apr�s 1989" 
     4 = "(NSP)" 
;

value sdnatpf 
     1 = "Nationalit� fran�aise" 
     2 = "Nationalit� �trang�re" 
     3 = "(NSP)" 
;

value sdpnaisptrf 
     1 = "France" 
     2 = "Pays europ�en" 
     3 = "Pays non europ�en" 
     4 = "Autres" 
     5 = "(NSP)" 
;

value sdnatmf 
     1 = "Nationalit� fran�aise" 
     2 = "Nationalit� �trang�re" 
     3 = "(NSP)" 
;

value sdpnaismtrf 
     1 = "France" 
     2 = "Pays europ�en" 
     3 = "Pays non europ�en" 
     4 = "Autres" 
     5 = "(NSP)" 
;

value sdnivief 
     1 = "1er quintile de niveau de vie" 
     2 = "2�me quintile de niveau de vie" 
     3 = "3�me quintile de niveau de vie" 
     4 = "4�me quintile de niveau de vie" 
     5 = "5�me quintile de niveau de vie" 
;

DATA  barometre_2000_2018_diff ;
set lib.barometre_2000_2018_diff;
FORMAT sdsexe sdsexef. ;
FORMAT habitat habitatf. ;
FORMAT region regionf. ;
FORMAT sdagetr sdagetrf. ;
FORMAT sdsitua sdsituaf. ;
FORMAT sdact sdactf. ;
FORMAT sdstat sdstatf. ;
FORMAT sdpcs7 sdpcs7f. ;
FORMAT sdpcs10 sdpcs10f. ;
FORMAT sdstatemp sdstatempf. ;
FORMAT sdsitfam sdsitfamf. ;
FORMAT sdpr sdprf. ;
FORMAT sdprsitua sdprsituaf. ;
FORMAT sdpract sdpractf. ;
FORMAT sdprstat sdprstatf. ;
FORMAT sdprpcs7 sdprpcs7f. ;
FORMAT sdprpcs10 sdprpcs10f. ;
FORMAT sdnbpers sdnbpersf. ;
FORMAT sdcouple sdcouplef. ;
FORMAT sdmatri sdmatrif. ;
FORMAT sdnbenf sdnbenff. ;
FORMAT sdnbadu sdnbaduf. ;
FORMAT sdsplit sdsplitf. ;
FORMAT sdmut sdmutf. ;
FORMAT og1 og1f. ;
FORMAT og2 og2f. ;
FORMAT og3_1 og3_1f. ;
FORMAT og3_2 og3_2f. ;
FORMAT me1 me1f. ;
FORMAT og4_1 og4_1f. ;
FORMAT og4_2 og4_2f. ;
FORMAT og4_3 og4_3f. ;
FORMAT og4_4 og4_4f. ;
FORMAT og4_5 og4_5f. ;
FORMAT og4_6 og4_6f. ;
FORMAT og4_7 og4_7f. ;
FORMAT og4_8 og4_8f. ;
FORMAT og4_9 og4_9f. ;
FORMAT og5_ab_1 og5_ab_1f. ;
FORMAT og5_ab_2 og5_ab_2f. ;
FORMAT og5_ab_3 og5_ab_3f. ;
FORMAT og5_ab_4 og5_ab_4f. ;
FORMAT og5_ab_5 og5_ab_5f. ;
FORMAT og5_ab_6 og5_ab_6f. ;
FORMAT og5_ab_7 og5_ab_7f. ;
FORMAT og5_cd_1 og5_cd_1f. ;
FORMAT og5_cd_4 og5_cd_4f. ;
FORMAT og5_cd_7 og5_cd_7f. ;
FORMAT og6 og6f. ;
FORMAT og7 og7f. ;
FORMAT og8_1 og8_1f. ;
FORMAT og8_2 og8_2f. ;
FORMAT og8_3 og8_3f. ;
FORMAT og8_4 og8_4f. ;
FORMAT og8_5 og8_5f. ;
FORMAT og8_6 og8_6f. ;
FORMAT og8_8 og8_8f. ;
FORMAT og8_9 og8_9f. ;
FORMAT og8_11 og8_11f. ;
FORMAT og9 og9f. ;
FORMAT og10 og10f. ;
FORMAT og11 og11f. ;
FORMAT og12 og12f. ;
FORMAT og13_1 og13_1f. ;
FORMAT og13_2 og13_2f. ;
FORMAT og13_3 og13_3f. ;
FORMAT og13_4 og13_4f. ;
FORMAT og13_5 og13_5f. ;
FORMAT og13_6 og13_6f. ;
FORMAT og13_7 og13_7f. ;
FORMAT og13_8 og13_8f. ;
FORMAT og13_9 og13_9f. ;
FORMAT og13_10 og13_10f. ;
FORMAT og13_11 og13_11f. ;
FORMAT og13bis_1 og13bis_1f. ;
FORMAT og13bis_2 og13bis_2f. ;
FORMAT og13bis_3 og13bis_3f. ;
FORMAT og13bis_4 og13bis_4f. ;
FORMAT og13bis_5 og13bis_5f. ;
FORMAT og13bis_6 og13bis_6f. ;
FORMAT og13bis_7 og13bis_7f. ;
FORMAT og13bis_8 og13bis_8f. ;
FORMAT og13bis_9 og13bis_9f. ;
FORMAT og13bis_10 og13bis_10f. ;
FORMAT og13bis_11 og13bis_11f. ;
FORMAT og13ter_1 og13ter_1f. ;
FORMAT og13ter_2 og13ter_2f. ;
FORMAT og13ter_3 og13ter_3f. ;
FORMAT og13ter_4 og13ter_4f. ;
FORMAT og13ter_5 og13ter_5f. ;
FORMAT og13ter_6 og13ter_6f. ;
FORMAT og13ter_7 og13ter_7f. ;
FORMAT og13ter_8 og13ter_8f. ;
FORMAT og13ter_9 og13ter_9f. ;
FORMAT og13ter_10 og13ter_10f. ;
FORMAT og13ter_11 og13ter_11f. ;
FORMAT in1 in1f. ;
FORMAT in2 in2f. ;
FORMAT in3 in3f. ;
FORMAT in4_ab in4_abf. ;
FORMAT in4_cd in4_cdf. ;
FORMAT in5_ab in5_abf. ;
FORMAT in5_cd in5_cdf. ;
FORMAT in6 in6f. ;
FORMAT in7 in7f. ;
FORMAT in8 in8f. ;
FORMAT in9 in9f. ;
FORMAT me2 me2f. ;
FORMAT in14_1 in14_1f. ;
FORMAT in14_2 in14_2f. ;
FORMAT in14_3 in14_3f. ;
FORMAT pe1 pe1f. ;
FORMAT pe2 pe2f. ;
FORMAT pe3 pe3f. ;
FORMAT pe5_1 pe5_1f. ;
FORMAT pe5_2 pe5_2f. ;
FORMAT pe5_3 pe5_3f. ;
FORMAT pe5_4 pe5_4f. ;
FORMAT pe5_5 pe5_5f. ;
FORMAT pe5_6 pe5_6f. ;
FORMAT pe4_1 pe4_1f. ;
FORMAT pe4_2 pe4_2f. ;
FORMAT pe4_3 pe4_3f. ;
FORMAT pe4_4 pe4_4f. ;
FORMAT pe6_1 pe6_1f. ;
FORMAT pe6_2 pe6_2f. ;
FORMAT pe6_3 pe6_3f. ;
FORMAT pe6_4 pe6_4f. ;
FORMAT pe9 pe9f. ;
FORMAT pe10 pe10f. ;
FORMAT pe17 pe17f. ;
FORMAT pe12 pe12f. ;
FORMAT pe13_ab pe13_abf. ;
FORMAT pe13_cd pe13_cdf. ;
FORMAT pe14_1 pe14_1f. ;
FORMAT pe14_2 pe14_2f. ;
FORMAT pe14_3 pe14_3f. ;
FORMAT pe14_4 pe14_4f. ;
FORMAT pe15 pe15f. ;
FORMAT me3 me3f. ;
FORMAT lo1 lo1f. ;
FORMAT lo2 lo2f. ;
FORMAT re1 re1f. ;
FORMAT re2 re2f. ;
FORMAT re3 re3f. ;
FORMAT re9bis re9bisf. ;
FORMAT re9 re9f. ;
FORMAT re10 re10f. ;
FORMAT fa1 fa1f. ;
FORMAT fa2 fa2f. ;
FORMAT fa3_ab fa3_abf. ;
FORMAT fa3_cd fa3_cdf. ;
FORMAT fa4 fa4f. ;
FORMAT fa5 fa5f. ;
FORMAT fa17 fa17f. ;
FORMAT fa6 fa6f. ;
FORMAT fa7 fa7f. ;
FORMAT fa8 fa8f. ;
FORMAT fa9_1 fa9_1f. ;
FORMAT fa9_2 fa9_2f. ;
FORMAT fa9_3 fa9_3f. ;
FORMAT fa9_4 fa9_4f. ;
FORMAT fa9_5 fa9_5f. ;
FORMAT fa10 fa10f. ;
FORMAT fa18 fa18f. ;
FORMAT fa11 fa11f. ;
FORMAT fa12 fa12f. ;
FORMAT fa13 fa13f. ;
FORMAT fa14 fa14f. ;
FORMAT fa15 fa15f. ;
FORMAT fa16 fa16f. ;
FORMAT ha1 ha1f. ;
FORMAT de1 de1f. ;
FORMAT de4 de4f. ;
FORMAT de5 de5f. ;
FORMAT de7 de7f. ;
FORMAT ps1_1 ps1_1f. ;
FORMAT ps1_2 ps1_2f. ;
FORMAT ps1_3 ps1_3f. ;
FORMAT ps1_4 ps1_4f. ;
FORMAT ps2 ps2f. ;
FORMAT ps3 ps3f. ;
FORMAT ps13_1 ps13_1f. ;
FORMAT ps13_2 ps13_2f. ;
FORMAT ps13_3 ps13_3f. ;
FORMAT ps13_4 ps13_4f. ;
FORMAT ps13_5 ps13_5f. ;
FORMAT ps13_6 ps13_6f. ;
FORMAT ps13_7 ps13_7f. ;
FORMAT ps15_1 ps15_1f. ;
FORMAT ps15_2 ps15_2f. ;
FORMAT ps15_3 ps15_3f. ;
FORMAT ps18 ps18f. ;
FORMAT sa1 sa1f. ;
FORMAT sa3 sa3f. ;
FORMAT sa4 sa4f. ;
FORMAT sa5 sa5f. ;
FORMAT sa6_ab_1 sa6_ab_1f. ;
FORMAT sa6_ab_2 sa6_ab_2f. ;
FORMAT sa6_ab_3 sa6_ab_3f. ;
FORMAT sa6_ab_4 sa6_ab_4f. ;
FORMAT sa6_ab_5 sa6_ab_5f. ;
FORMAT sa6_ab_6 sa6_ab_6f. ;
FORMAT sa6_cd_1 sa6_cd_1f. ;
FORMAT sa6_cd_2 sa6_cd_2f. ;
FORMAT sa6_cd_3 sa6_cd_3f. ;
FORMAT sa6_cd_4 sa6_cd_4f. ;
FORMAT sa6_cd_5 sa6_cd_5f. ;
FORMAT sa6_cd_6 sa6_cd_6f. ;
FORMAT me4 me4f. ;
FORMAT sa7_1 sa7_1f. ;
FORMAT sa7_2 sa7_2f. ;
FORMAT sa7_3 sa7_3f. ;
FORMAT sa7_4 sa7_4f. ;
FORMAT sa7_5 sa7_5f. ;
FORMAT sa7_6 sa7_6f. ;
FORMAT sa7_7 sa7_7f. ;
FORMAT sa7_8 sa7_8f. ;
FORMAT sa8_1 sa8_1f. ;
FORMAT sa8_2 sa8_2f. ;
FORMAT sa8_3 sa8_3f. ;
FORMAT sa8_5 sa8_5f. ;
FORMAT sa8_6 sa8_6f. ;
FORMAT sa8_7 sa8_7f. ;
FORMAT sa8_9 sa8_9f. ;
FORMAT sa9_1 sa9_1f. ;
FORMAT sa9_2 sa9_2f. ;
FORMAT sa9_3 sa9_3f. ;
FORMAT sa9_4 sa9_4f. ;
FORMAT sa9_5 sa9_5f. ;
FORMAT sa9_6 sa9_6f. ;
FORMAT sa22_1 sa22_1f. ;
FORMAT sa22_2 sa22_2f. ;
FORMAT sa22_3 sa22_3f. ;
FORMAT sa22_4 sa22_4f. ;
FORMAT sa22_5 sa22_5f. ;
FORMAT sa22_6 sa22_6f. ;
FORMAT sa22_7 sa22_7f. ;
FORMAT sa22_8 sa22_8f. ;
FORMAT sa26_1 sa26_1f. ;
FORMAT sa26_2 sa26_2f. ;
FORMAT cs1 cs1f. ;
FORMAT cs24 cs24f. ;
FORMAT cs4 cs4f. ;
FORMAT cs5_ab cs5_abf. ;
FORMAT cs5_cd cs5_cdf. ;
FORMAT cs17 cs17f. ;
FORMAT cs18 cs18f. ;
FORMAT cs19_1 cs19_1f. ;
FORMAT cs19_2 cs19_2f. ;
FORMAT cs20 cs20f. ;
FORMAT cs13 cs13f. ;
FORMAT cs14 cs14f. ;
FORMAT cs9 cs9f. ;
FORMAT cs21_1 cs21_1f. ;
FORMAT cs21_2 cs21_2f. ;
FORMAT cs21_3 cs21_3f. ;
FORMAT cs21_4 cs21_4f. ;
FORMAT cs21_5 cs21_5f. ;
FORMAT cs21_6 cs21_6f. ;
FORMAT cs21_7 cs21_7f. ;
FORMAT cs22 cs22f. ;
FORMAT cs23 cs23f. ;
FORMAT sdassynd_1 sdassynd_1f. ;
FORMAT sdassynd_2 sdassynd_2f. ;
FORMAT sdrel sdrelf. ;
FORMAT sdproxim_1 sdproxim_1f. ;
FORMAT sdproxim_2 sdproxim_2f. ;
FORMAT sdproxim_3 sdproxim_3f. ;
FORMAT sdproxim_4 sdproxim_4f. ;
FORMAT sdproxim_5 sdproxim_5f. ;
FORMAT sdproxim_6 sdproxim_6f. ;
FORMAT sdproxim_7 sdproxim_7f. ;
FORMAT sdproxim_8 sdproxim_8f. ;
FORMAT sdproxim_9 sdproxim_9f. ;
FORMAT sdrichom_ab sdrichom_abf. ;
FORMAT sdrichom_cd_1 sdrichom_cd_1f. ;
FORMAT sdrichom_cd_2 sdrichom_cd_2f. ;
FORMAT sdres_1 sdres_1f. ;
FORMAT sdres_2 sdres_2f. ;
FORMAT sdres_3 sdres_3f. ;
FORMAT sdres_4 sdres_4f. ;
FORMAT sdres_5 sdres_5f. ;
FORMAT sdres_6 sdres_6f. ;
FORMAT sdres_7 sdres_7f. ;
FORMAT sdres_8 sdres_8f. ;
FORMAT sdres_9 sdres_9f. ;
FORMAT sdres_10 sdres_10f. ;
FORMAT sdres_11 sdres_11f. ;
FORMAT sdres_12 sdres_12f. ;
FORMAT sdrevtr sdrevtrf. ;
FORMAT me5 me5f. ;
FORMAT me6 me6f. ;
FORMAT sddipl sddiplf. ;
FORMAT sdpol sdpolf. ;
FORMAT sdpoltr sdpoltrf. ;
FORMAT sdnat sdnatf. ;
FORMAT sdpnaistr sdpnaistrf. ;
FORMAT sdanfrtr sdanfrtrf. ;
FORMAT sdnatp sdnatpf. ;
FORMAT sdpnaisptr sdpnaisptrf. ;
FORMAT sdnatm sdnatmf. ;
FORMAT sdpnaismtr sdpnaismtrf. ;
FORMAT sdnivie sdnivief. ;
RUN;
