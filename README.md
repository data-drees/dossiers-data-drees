# Dossiers de Data DREES

Ce projet contient un export des données de [data Drees](http://www.data.drees.sante.gouv.fr/ReportFolders/reportFolders.aspx?sCS_referer=&sCS_ChosenLang=fr) au 17 juin 2020.

Cet export public est ponctuel. 

Il a pour but faciliter les échanges sur les fichiers et leurs organisation, durant un travail de refonte du site de diffusion. 

**Note : Merci de *ne pas clonner ce projet* en local.** 
Le dossier est volumineux, et cela impacterait le quota GitLab du groupe DREES_code.

## Contact

Pour toute question ou remarque, merci de contacter [ld-lab-github@sante.gouv.fr](ld-lab-github@sante.gouv.fr).